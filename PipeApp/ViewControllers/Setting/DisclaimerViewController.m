//
//  DisclaimerViewController.m
//  PipeApp
//
//  Created by Redbytes on 01/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DisclaimerViewController.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Disclaimer";
   
    UIFont *maintitleFont = [UIFont boldSystemFontOfSize:15];
    //                             fontWithName:@"Arial" size:15.0f];
    UIFont *subFont = [UIFont fontWithName:@"Arial" size:13.0f];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        NSMutableAttributedString *Legalnotice = [[NSMutableAttributedString alloc]initWithString:@"Rechtlicher Hinweis\nPersonen, welche die in diesen Mobilgeräteapplikationen (App) veröffentlichten Informationen abrufen, erklären sich mit den nachstehenden Nutzungsbedingungen einverstanden.\n"];
        [Legalnotice addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,Legalnotice.length-(Legalnotice.length-20))];
        [Legalnotice addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(20,Legalnotice.length-20)];
        
        
        NSMutableAttributedString *Liabilitydisclaimer = [[NSMutableAttributedString alloc]initWithString:@"Haftungsausschluss\nDer Inhalt dieser Mobilgeräteapplikation (App) dient ausschliesslich zu Informationszwecken und kann jederzeit und ohne vorherige Ankündigung geändert werden. Der App-Inhaber lehnt jegliche Haftung für Schäden oder Folgeschäden ab, welche sich aus der Benutzung oder dem Zugriff auf die App (bzw. aus der Unmöglichkeit der Benutzung oder des Zugriffs) ergeben.\n"];
        [Liabilitydisclaimer addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,Liabilitydisclaimer.length-(Liabilitydisclaimer.length-18))];
        [Liabilitydisclaimer addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(18,Liabilitydisclaimer.length-18)];
        
        
        NSMutableAttributedString *personalInformation = [[NSMutableAttributedString alloc]initWithString:@"Persönliche Daten\nEine Ausnahme bilden freiwillig eingegebene persönliche Daten, etwa bei der Kontaktaufnahme, bei Bestellvorgängen oder der Abonnierung von Newslettern. Diese Daten werden in einer Datenbank gespeichert. Die Daten werden vertraulich behandelt und nicht an Dritte weitergegeben.\n"];
        [personalInformation addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,personalInformation.length-(personalInformation.length-17))];
        [personalInformation addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(17,personalInformation.length-17)];
        
        
        NSMutableAttributedString *copyright = [[NSMutableAttributedString alloc]initWithString:@"Urheberrecht / Leistungsschutzrecht\nDie auf dieser App veröffentlichten Inhalte und bereitgestellten Informationen (Texte, Zeichnungen, Bilder, Grafiken, Ton-, Video-, und Animationsdateien) unterliegen dem Urheberrecht und Leistungsschutzrecht. Jede Art der Vervielfältigung, Bearbeitung, Verbreitung, Einspeicherung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechts bedarf der vorherigen schriftlichen Zustimmung der Brugg Rohrsystem AG, Schweiz als Rechtinhaber. Das unerlaubte Kopieren/Speichern der bereitgestellten Informationen  ist nicht gestattet und strafbar.\n"];
        [copyright addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,copyright.length-(copyright.length-36))];
        [copyright addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(36,copyright.length-36)];
        
        
        NSMutableAttributedString *disclaimer = [[NSMutableAttributedString alloc]initWithAttributedString:Legalnotice];
        // [disclaimer appendAttributedString:Legalnotice];
        [disclaimer appendAttributedString:Liabilitydisclaimer];
        [disclaimer appendAttributedString:personalInformation];
        [disclaimer appendAttributedString:copyright];
        self.disclaimerTextView.attributedText = disclaimer;
    }
    else{
        [Language setLanguage:@"en"];
        
    NSMutableAttributedString *Legalnotice = [[NSMutableAttributedString alloc]initWithString:@"Legal notice\n Persons who access information published in this mobile device application (app) hereby declare that they agree with the following conditions of use.\n"];
    [Legalnotice addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,Legalnotice.length-(Legalnotice.length-12))];
    [Legalnotice addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(12,Legalnotice.length-12)];
    
    
    NSMutableAttributedString *Liabilitydisclaimer = [[NSMutableAttributedString alloc]initWithString:@"Liability disclaimer\nThe content of this mobile device application (app) is provided exclusively for information purposes and can be changed at any time and without prior notice. The app owner declines any liability for damages or consequential losses which result from the use or access of the app (or from the impossibility of use or access).\n"];
    [Liabilitydisclaimer addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,Liabilitydisclaimer.length-(Liabilitydisclaimer.length-20))];
    [Liabilitydisclaimer addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(20,Liabilitydisclaimer.length-20)];
    
    
    NSMutableAttributedString *personalInformation = [[NSMutableAttributedString alloc]initWithString:@"Personal information\nAn exception is voluntarily supplied personal information, given for example when making contact, placing orders or subscribing to newsletters. This data is stored in a database. The data is treated confidentially and not given to third parties.\n"];
    [personalInformation addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,personalInformation.length-(personalInformation.length-20))];
    [personalInformation addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(20,personalInformation.length-20)];
    
    
    NSMutableAttributedString *copyright = [[NSMutableAttributedString alloc]initWithString:@"Copyright law / ancilliary copyright\nContent and information published in this app (texts, drawings, images, graphics, audio, video and animated files) are subject to copyright law and ancillary copyright. Any form of reproduction, processing, editing, storage and any form of reuse outside of the boundaries of copyright law requires the previous written agreement of Brugg Rohrsystem AG, Switzerland, the copyright holder. Unauthorized copying/storage of the information provided is not permitted and is punishable by law.\n"];
    [copyright addAttribute:NSFontAttributeName value:maintitleFont  range:NSMakeRange(0,copyright.length-(copyright.length-37))];
    [copyright addAttribute:NSFontAttributeName value:subFont  range:NSMakeRange(37,copyright.length-37)];

    
  NSMutableAttributedString *disclaimer = [[NSMutableAttributedString alloc]initWithAttributedString:Legalnotice];
   // [disclaimer appendAttributedString:Legalnotice];
    [disclaimer appendAttributedString:Liabilitydisclaimer];
    [disclaimer appendAttributedString:personalInformation];
    [disclaimer appendAttributedString:copyright];
    self.disclaimerTextView.attributedText = disclaimer;
    }
   self.disclaimerTextView.textAlignment = NSTextAlignmentJustified;
        
   // [myKeysButtonText addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,7)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
