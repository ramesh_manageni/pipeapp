//
//  PipeTypes.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PipeVariableDetail;

@interface PipeTypes : NSManagedObject

@property (nonatomic, retain) NSString * pipeName;
@property (nonatomic, retain) NSSet *pipeVariableDetail;
@end

@interface PipeTypes (CoreDataGeneratedAccessors)

- (void)addPipeVariableDetailObject:(PipeVariableDetail *)value;
- (void)removePipeVariableDetailObject:(PipeVariableDetail *)value;
- (void)addPipeVariableDetail:(NSSet *)values;
- (void)removePipeVariableDetail:(NSSet *)values;

@end
