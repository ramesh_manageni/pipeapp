//
//  ShowDocumentViewController.h
//  PipeApp
//
//  Created by Redbytes on 10/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Document.h"
#import<MessageUI/MessageUI.h>

@interface ShowDocumentViewController : UIViewController<AVAudioPlayerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UILabel *docNameTextField;
@property (strong, nonatomic) IBOutlet UIView *docBaseView;
@property (strong, nonatomic) IBOutlet UIView *buttonBaseView;
@property (strong, nonatomic) IBOutlet UIImageView *docImageView;
@property (strong, nonatomic) IBOutlet UIButton *voiceRecordButton;
@property (strong, nonatomic)  Document *docRecord;

- (IBAction)voiceButtonClicked:(id)sender;
- (IBAction)shareButtonClicked:(id)sender;
- (IBAction)deleteDocClicked:(id)sender;

@end
