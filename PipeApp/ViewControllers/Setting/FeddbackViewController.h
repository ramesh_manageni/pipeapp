//
//  FeddbackViewController.h
//  PipeApp
//
//  Created by Redbytes on 01/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<MessageUI/MessageUI.h>

@interface FeddbackViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *nemeTextfield;
@property (strong, nonatomic) IBOutlet UITextField *emailTextfield;
@property (strong, nonatomic) IBOutlet UITextView *remarkTextView;

@property (strong, nonatomic) IBOutlet UIButton *sendButton;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *defaultLabel;

@property (strong, nonatomic) NSString *docRecordName;

- (IBAction)sendButtonClicked:(id)sender;
@end
