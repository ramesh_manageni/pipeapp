//
//  ProductLoginViewController.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 1/28/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductLoginViewController : UIViewController<UITextFieldDelegate>
//passworTextField,loginButton,,loginButtonClicked

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *passworTextField;
- (IBAction)loginButtonClicked:(id)sender;

@end
