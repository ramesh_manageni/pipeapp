//
//  PipeRoughness.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PipeRoughness : NSManagedObject

@property (nonatomic, retain) NSString * pipeName;
@property (nonatomic, retain) NSNumber * surfaceRoughness;

@end
