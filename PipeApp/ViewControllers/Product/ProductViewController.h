//
//  ProductViewController.h
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BarViewController.h"

@interface ProductViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
