//
//  DocumentCustomCell.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 12/29/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DocumentCustomCell.h"

@implementation DocumentCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
