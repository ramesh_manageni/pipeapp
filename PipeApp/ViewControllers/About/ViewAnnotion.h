//
//  ViewAnnotion.h
//  Assignement_MapView
//
//  Created by RedBytes on 19/03/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewAnnotion : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end
