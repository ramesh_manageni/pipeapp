//
//  ProductListViewController.h
//  PipeApp
//
//  Created by Redbytes on 21/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKSTableView.h"
#import "ProductDetailsViewController.h"


@interface ProductListViewController : UIViewController<SKSTableViewDelegate>

@property (nonatomic, weak) IBOutlet SKSTableView *tableView;
@property(nonatomic, strong) NSArray *productImageArray;
@property (weak, nonatomic) IBOutlet UILabel *productDetailLabel;

@end
