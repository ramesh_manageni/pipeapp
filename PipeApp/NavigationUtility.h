//
//  NavigationUtility.h
//  Service48
//
//  Created by Redbytes on 13/05/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NavigationUtility : NSObject
//Create Right bar button item
+(UIBarButtonItem *)createRightBarButtonForViewController:(UIViewController *)targetViewController withTitle:(NSString *)title withImage:(NSString *)imageName;

//Create Left bar button item

+(UIBarButtonItem *)createLeftBarButtonForViewController:(UIViewController *)targetViewController withTitle:(NSString *)title withImage:(NSString *)imageName;

//Create navigation bar title.
+(UILabel *)setNavigationTitle:(NSString *) title withFontNmae:(NSString *)fontName andSize:(double )size;

@end
