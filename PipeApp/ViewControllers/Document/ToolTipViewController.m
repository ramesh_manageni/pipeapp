

//
//  ToolTipViewController.m
//  temp
//
//  Created by Shaikh Rizwan on 11/21/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import "ToolTipViewController.h"
#import "SeeImageViewController.h"
#import "UIImage+ScreenShots.h"


@interface ToolTipViewController ()
{
    UIImageView *imgView,*RessultimgView;
    float x,y;
    int tag;
    
    NSMutableArray *RecordArray;
    NSString *textFieldText;
    CLLocationManager *_locationManager;
    NSArray *locationArray;
}
@property (nonatomic, strong)	NSArray			*colorSchemes;
@property (nonatomic, strong)	NSDictionary	*contents;
@property (nonatomic, strong)	id				currentPopTipViewTarget;
@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;
@property (nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation ToolTipViewController
@synthesize locationManager=_locationManager;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
        [super viewDidLoad];
    [self.seeImgButton.layer setCornerRadius:5.0];
    [self.seeImgButton setUserInteractionEnabled:YES];
    RecordArray = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    imgView=[[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-250)/2, 80, 250, 250)];
    [imgView setUserInteractionEnabled:YES];
    [self.view addSubview:imgView];
    NSLog(@"%@",DocumentImage);
    imgView.image=DocumentImage;//[UIImage imageNamed:@"CCo.png"];
    textFieldText=@"";
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [imgView addGestureRecognizer:tapRecognizer];
    [self defaultTapOnImage];
    
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self ];
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];

}

-(void)viewWillAppear:(BOOL)animated
{
    [self localizationAction];
    [self.seeImgButton setTitle:[Language get:@"See image" alter:@"Title not found"] forState:UIControlStateNormal];
}
-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        
    }
    else{
        [Language setLanguage:@"en"];
        
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tapAction:(UITapGestureRecognizer *)gesture
{
    NSLog(@"single-tap");
    CGPoint touchLocation = [gesture locationInView:imgView];
    NSLog(@"%f %f", [gesture locationInView:imgView].x,  [gesture locationInView:imgView].y);

    x = touchLocation.x;
     y = touchLocation.y;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, 44, 44)];
//    button.backgroundColor=[UIColor blackColor];
    [button setBackgroundImage:[UIImage imageNamed:@"dot.png"] forState:UIControlStateNormal];
    [imgView addSubview:button];
     UITextField *subtextField=   [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];//0,0,100,20;
    subtextField.delegate=self;
    float temp =(x+y)/2;
    tag=[[NSNumber numberWithInt:temp]intValue  ];
    button.tag=[[NSNumber numberWithInt:temp]intValue  ];
    
    
    [button addTarget:self action:@selector(dotButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [ self  toolTipButtonAction:button TextFiled:subtextField];
    
    }
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)dismissAllPopTipViews
{
	while ([self.visiblePopTipViews count] > 0) {
		CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
		[popTipView dismissAnimated:YES];
		[self.visiblePopTipViews removeObjectAtIndex:0];
	}
}
#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
	[self.visiblePopTipViews removeObject:popTipView];
	self.currentPopTipViewTarget = nil;
}

#pragma mark - UIViewController methods

- (void)willAnimateRotationToInterfaceOrientation:(__unused UIInterfaceOrientation)toInterfaceOrientation duration:(__unused NSTimeInterval)duration
{
	for (CMPopTipView *popTipView in self.visiblePopTipViews) {
		id targetObject = popTipView.targetObject;
		[popTipView dismissAnimated:NO];
		
		if ([targetObject isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)targetObject;
			[popTipView presentPointingAtView:button inView:self.view animated:NO];
		}
		else {
			UIBarButtonItem *barButtonItem = (UIBarButtonItem *)targetObject;
			[popTipView presentPointingAtBarButtonItem:barButtonItem animated:NO];
		}
	}
}

- (void)toolTipButtonAction:(id)sender TextFiled:(UITextField *)textField
{
    [self dismissAllPopTipViews];
    textField.font=[UIFont systemFontOfSize:15];
	
	if (sender == self.currentPopTipViewTarget) {
		// Dismiss the popTipView and that is all
		self.currentPopTipViewTarget = nil;
	}
    
    
    
    else {
        
        NSString *contentMessage = nil;
        UIView *contentView = nil;
		//NSNumber *key = [NSNumber numberWithInteger:[(UIView *)sender tag]];
		id content = textField;
		if ([content isKindOfClass:[UIView class]]) {
			contentView = content;
		}
        
        else
        {
			contentMessage = content;
        }
		
        
        //
        //		NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //		UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //		UIColor *textColor = [colorScheme objectAtIndex:1];
		
        
		CMPopTipView *popTipView;
        
        UIButton *crossbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [crossbutton setFrame:CGRectMake(3,8, 15,15)];
       // button.backgroundColor=[UIColor redColor];
        [crossbutton setBackgroundImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
        
        [crossbutton addTarget:self action:@selector(crossButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *donebutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [donebutton setFrame:CGRectMake(125,8, 15,15)];//105,8, 15,15
        // button.backgroundColor=[UIColor redColor];
        [donebutton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
        
        [donebutton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
		
        if (contentView) {
			popTipView = [[CMPopTipView alloc] initWithCustomView:contentView];
            
            [popTipView addSubview:donebutton];
            [popTipView addSubview:crossbutton];
            
            popTipView.backgroundColor=[UIColor whiteColor];
        
            

		}
        else{
            
            
			popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        }
		
		//popTipView.delegate1 = self;
        popTipView.delegate=self;
		
		      
        popTipView.animation = arc4random() % 2;
		popTipView.has3DStyle = (BOOL)(arc4random() % 2);
		
		popTipView.dismissTapAnywhere = YES;
        //[popTipView autoDismissAnimated:YES atTimeInterval:8.0];
        
		if ([sender isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)sender;
			[popTipView presentPointingAtView:button inView:self.view animated:YES];
		}
		
		
		[self.visiblePopTipViews addObject:popTipView];
		self.currentPopTipViewTarget = sender;
	}
    
    
    
}
-(void)crossButtonAction:(UIButton *)sender
{
    // code for removing if user wantto clear tooltip.
    
//        for (int i = 0; i< [RecordArray count];i++) {
//            if ([[[RecordArray objectAtIndex:i]valueForKey:@"tag"]isEqualToString:[NSString stringWithFormat:@"%d",tag]])
//            {
//                [RecordArray removeObjectAtIndex:i];
//        }
//    }
    CMPopTipView *obj = (CMPopTipView *)[sender superview];
    [obj dismissAnimated:YES];
    sender = (UIButton *)[self.view viewWithTag:tag];
    [sender removeFromSuperview];
    
    //code for taking screen shot of image without Clicking the seeimage button
//    [self createImageFromTooltipView];
//    [RessultimgView removeFromSuperview];
//        [self.view addSubview:imgView];

    
    
}
-(void)doneButtonAction:(UIButton *)sender
{
    [self.view endEditing:YES];
   
//    UITextField *obj1 = (UITextField *)[sender superview];
//    if ([obj1 isKindOfClass:[UIView class]]) {
//        UITextField * contentView = (UITextField *)obj1;
//                [contentView resignFirstResponder];
//        
//    }
       if (![textFieldText isEqualToString:@""]) {

        NSArray *keys = [NSArray arrayWithObjects:@"x", @"y",@"tag",@"textFieldText", nil];
        NSArray *objects = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%f",x],[NSString stringWithFormat:@"%f",y], [NSString stringWithFormat:@"%d",tag],textFieldText ,nil];
        
        NSDictionary *register1 = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        [RecordArray addObject:register1];
           textFieldText = @"";
       }
    
    

    CMPopTipView *obj = (CMPopTipView *)[sender superview];
    
    [obj dismissAnimated:YES];
    
    
    //code for taking screen shot of image without Clicking the seeimage button
    
    //[self createImageFromTooltipView];
}

-(void)dotButtonAction:(UIButton * )sender
{
    tag =sender.tag;
    
    UITextField *subtextField=   [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
    subtextField.delegate=self;
    
    for (int i=0; i<[RecordArray count]; i++) {
        
        if (sender.tag ==[[[RecordArray objectAtIndex:i] valueForKey:@"tag"] intValue])
        {
            subtextField.text=[[RecordArray objectAtIndex:i] valueForKey:@"textFieldText"];
        }
        
        
    }
    
    [self toolTipButtonAction:sender TextFiled:subtextField];
    
}
-(void)defaultTapOnImage
{
    x=75;
    y =75;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, 44, 44)];
    //    button.backgroundColor=[UIColor blackColor];
    [button setBackgroundImage:[UIImage imageNamed:@"dot.png"] forState:UIControlStateNormal];
    [imgView addSubview:button];
    UITextField *subtextField=   [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];//0,0,100,20
    subtextField.delegate=self;
    float temp =(x+y)/2;
    tag=[[NSNumber numberWithInt:temp]intValue  ];
    button.tag=[[NSNumber numberWithInt:temp]intValue  ];
    
    [button addTarget:self action:@selector(dotButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [ self  toolTipButtonAction:button TextFiled:subtextField];

}
#pragma  mark textField Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
        //new code
    if (![textField.text isEqualToString:@""])
    {
        textFieldText=textField.text;
    }
    
   
    [textField resignFirstResponder];
}

- (IBAction)seeImageButtonAction:(id)sender {
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SeeImageViewController *objSeeImageViewController = [main instantiateViewControllerWithIdentifier:@"SeeImageViewController"];
    objSeeImageViewController.fetchArray = [[NSMutableArray alloc]initWithArray:RecordArray];
    objSeeImageViewController.locationArray = locationArray;
    DocumentImage = imgView.image;
    [self.navigationController pushViewController:objSeeImageViewController animated:YES];

}
#pragma mark seeImageviewController Method for screenshot


//code for taking screen shot of image without Clicking the seeimage button
-(void)createImageFromTooltipView
{
    
    RessultimgView=[[UIImageView alloc] initWithFrame:CGRectMake(50, 120, 250, 250)];
    [RessultimgView setUserInteractionEnabled:YES];
    RessultimgView.image = imgView.image;
    [imgView removeFromSuperview];
    
    [self.view addSubview:RessultimgView];
    //RessultimgView.image = DocumentImage;
    
    for (int i=0; i<[RecordArray count]; i++) {
        CGFloat x1=[[[RecordArray objectAtIndex:i] valueForKey:@"x"] floatValue];
        CGFloat y1=[[[RecordArray objectAtIndex:i] valueForKey:@"y"] floatValue];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(x1, y1, 5, 5)];
        button.backgroundColor=[UIColor blackColor];
        button.tag=[[[RecordArray objectAtIndex:i] valueForKey:@"tag"] intValue];
//        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [RessultimgView addSubview:button];
        UILabel *textLabel=   [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
        textLabel.font=[UIFont systemFontOfSize:12];
        [self toolTipButtonAction1:button TextLabel:textLabel];
        
        
        DocumentImage = [self toolTipsScreenShots];
        [RessultimgView removeFromSuperview];
        [self.view addSubview:imgView];

    }

}
- (void)toolTipButtonAction1:(UIButton *)sender TextLabel:(UILabel *)textLabel
{
    [self dismissAllPopTipViews];
	
	if (sender == self.currentPopTipViewTarget) {
		// Dismiss the popTipView and that is all
		self.currentPopTipViewTarget = nil;
	}
    
    
    
    else {
        
        NSString *contentMessage = nil;
        UIView *contentView = nil;
		//NSNumber *key = [NSNumber numberWithInteger:[(UIView *)sender tag]];
        
        
		
        
        
        for (int i=0; i<[RecordArray count]; i++) {
            
            if (sender.tag ==[[[RecordArray objectAtIndex:i] valueForKey:@"tag"] intValue])
            {
                textLabel.text=[[RecordArray objectAtIndex:i] valueForKey:@"textFieldText"];
            }
            
            
        }
        
        id content = textLabel;
        
		if ([content isKindOfClass:[UIView class]]) {
			contentView = content;
		}
        
        else
        {
			contentMessage = content;
        }
		
        
        //
        //		NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //		UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //		UIColor *textColor = [colorScheme objectAtIndex:1];
		
        
		CMPopTipView *popTipView;
        //
        //        UIButton *crossbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        //        [crossbutton setFrame:CGRectMake(205,13, 15,15)];
        //        // button.backgroundColor=[UIColor redColor];
        //        [crossbutton setBackgroundImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
        //
        //        [crossbutton addTarget:self action:@selector(crossButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
		
        if (contentView) {
			popTipView = [[CMPopTipView alloc] initWithCustomView:contentView];
            //[popTipView addSubview:crossbutton];
            
            
            
		}
        else{
            
            
			popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        }
		
		//popTipView.delegate1 = self;
        popTipView.delegate=self;
		popTipView.backgroundColor=[UIColor clearColor];
        
        popTipView.animation = arc4random() % 2;
		popTipView.has3DStyle = (BOOL)(arc4random() % 2);
		
		popTipView.dismissTapAnywhere = YES;
        //[popTipView autoDismissAnimated:YES atTimeInterval:8.0];
        
		if ([sender isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)sender;
			[popTipView presentPointingAtView:button inView:RessultimgView animated:YES];
		}
		
		
		[self.visiblePopTipViews addObject:popTipView];
		self.currentPopTipViewTarget = sender;
	}
    
    
    
}
-(UIImage *)toolTipsScreenShots
{
    UIImage *viewImage = [UIImage imageFromView:RessultimgView];
    
    return viewImage;
}
#pragma mark location manger delgate method
- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    locationArray = locations;
    // Current Location
    CLLocation *currentLocation = [locations objectAtIndex:0];
    //currentLocation.coordinate.latitude=47.574435;
    NSLog(@"currentLocation=%@",currentLocation);
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = currentLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
//    latLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
//    longLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    //[self.mapView addAnnotation:point];
    
    
    
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location manager fails");
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation   *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"Location manager work");
}


@end
