//
//  DashBoardViewController.m
//  PipeApp
//
//  Created by Redbytes on 25/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DashBoardViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface DashBoardViewController ()


@end



@implementation DashBoardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    permission = NO;
	// Do any additional setup after loading the view.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
   
   

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [self.scrollView setContentSize:CGSizeMake(screenWidth, screenHeight)];
    self.scrollView.showsVerticalScrollIndicator=NO;

    UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [imgView setUserInteractionEnabled:YES];
    [self.view addSubview:imgView];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [imgView addGestureRecognizer:tapRecognizer];
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (scale == 2.0f)
        {
            if (pixelHeight == 960.0f)
            {
                imgView.image=[UIImage imageNamed:@"splashscreen4.png"];
            }
            
            else if (pixelHeight == 1136.0f)
            {
                imgView.image=[UIImage imageNamed:@"splashscreen4.png"];
                
            }
        }
    }
   [self checkForIphoneSize];
    
}

-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        
        [self.productButton setBackgroundImage:[UIImage imageNamed:@"produkte"] forState:UIControlStateNormal];
        
        [self.productDocumentButton setBackgroundImage:[UIImage imageNamed:@"ProdukteDokumentation"] forState:UIControlStateNormal];
        
        [self.calculatorButton setBackgroundImage:[UIImage imageNamed:@"Kalkulator"] forState:UIControlStateNormal];
        
        [self.settingButton setBackgroundImage:[UIImage imageNamed:@"Einstellung"] forState:UIControlStateNormal];
        
        [self.aboutButton setBackgroundImage:[UIImage imageNamed:@"Über" ] forState:UIControlStateNormal];

        
    }
    else{
        [Language setLanguage:@"en"];
        [self.productButton setBackgroundImage:[UIImage imageNamed:@"ProductButton"] forState:UIControlStateNormal];
        
        [self.productDocumentButton setBackgroundImage:[UIImage imageNamed:@"Product Documentation"] forState:UIControlStateNormal];
        
        [self.calculatorButton setBackgroundImage:[UIImage imageNamed:@"Calculator_button"] forState:UIControlStateNormal];
        
        [self.settingButton setBackgroundImage:[UIImage imageNamed:@"Settings"] forState:UIControlStateNormal];
        
        [self.aboutButton setBackgroundImage:[UIImage imageNamed:@"About" ] forState:UIControlStateNormal];
        

        
    }
    
    
}
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    [tap.view removeFromSuperview];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self localizationAction];
    [self.tabBarController.tabBar setHidden:YES];
     self.navigationItem.title = [Language get:@"Dashboard" alter:@"Title not found" ];
    NSLog(@"%@",[Language get:@"ProductButton" alter:@"Title not found" ]);

    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)productButtonClicked:(id)sender
{
    
//    NSString *password = [[NSUserDefaults standardUserDefaults]
//                            stringForKey:@"password"];
//    if ([password isEqualToString:@""]) {
//        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        LoginViewController *objLogin = [main instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        controllerTag = @"1";
//        [self.navigationController pushViewController:objLogin animated:YES];
//    }
//    else{
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appdelegte.objTab setSelectedIndex:2];
    
    
    
}

- (IBAction)documentButtonClicked:(id)sender
{
    controllerTag = @"2";
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appdelegte.objTab setSelectedIndex:3];
    
}

- (IBAction)calculatorButtonClicked:(id)sender
{
    
//    NSString *password = [[NSUserDefaults standardUserDefaults]
//                          stringForKey:@"password"];
//    if ([password isEqualToString:@""]) {
//    
//    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LoginViewController *objLogin = [main instantiateViewControllerWithIdentifier:@"LoginViewController"];
//    controllerTag = @"3";
//    [self.navigationController pushViewController:objLogin animated:YES];
//    }
//    else
//    {
    
        AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSLog(@"selectedIndex=%lu",(unsigned long)appdelegte.objTab.selectedIndex);
        [appdelegte.objTab setSelectedIndex:1];

   // }

}

- (IBAction)settingButtonClicked:(id)sender
{
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appdelegte.objTab setSelectedIndex:4];
    
}

- (IBAction)aboutButtonClicked:(id)sender
{
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appdelegte.objTab setSelectedIndex:5];
    
}
-(void)checkForIphoneSize
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 568){
                // iPhone retina-4 inch
                
            } else{
                // iPhone retina-3.5 inch
                NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
                
                
            }
        }
        else {
            // not retina display
        }
    }
    
    
    
    
    
}

@end
