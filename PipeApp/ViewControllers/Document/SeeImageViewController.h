//
//  SeeImageViewController.h
//  temp
//
//  Created by Shaikh Rizwan on 11/25/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"
#import <CoreLocation/CoreLocation.h>

@protocol seeImageProtocol <NSObject>

-(void)Takeimage:(UIImageView *)imageview;

@end
@interface SeeImageViewController : UIViewController<CMPopTipViewDelegate,CLLocationManagerDelegate>
@property(strong,nonatomic)NSMutableArray *fetchArray;
@property(strong,nonatomic)NSArray *locationArray;
//- (IBAction)screenshotAction:(id)sender;
//@property (weak, nonatomic) IBOutlet UIImageView *screenshotImageview;
@end
