//
//  ProductViewController.m
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ProductViewController.h"
#import "DBManager.h"


@interface ProductViewController ()

@property(nonatomic,strong)NSArray *tempArray;

@end

@implementation ProductViewController

@synthesize tempArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
//    NSString *password = [[NSUserDefaults standardUserDefaults]
//                          stringForKey:@"password"];
//    
//    if ([password isEqualToString:@""]) {
//        AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
//        [appdelegte.objTab setSelectedIndex:0];
//    }
//    else
//    {

    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [self addRightBarButton];
	// Do any additional setup after loading the view.
   
    tempArray=[[DBManager getSharedInstance] getMaxTemp];

    //}
}

-(void)viewWillAppear:(BOOL)animated
{
    [self localizationAction];
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    
    if (![password isEqualToString:@"Bru99"]) {
        [self performActionOnPassword];
    }
    else{
    
     self.navigationItem.title =[Language get:@"Product" alter:@"Title not found"];
    self.navigationItem.hidesBackButton = YES;
    [self.tabBarController.tabBar setHidden:NO];
    UILabel *productLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, self.view.frame.size.width, 35)];
    productLabel.backgroundColor = [UIColor whiteColor]; 
    productLabel.text =[Language get:@"Please choose desired value" alter:@"title not found"] ;//Bitte gewünschten Wert auswählen
    
    productLabel.layer.borderColor = [UIColor blackColor].CGColor;
    productLabel.layer.borderWidth = 1.0;
    productLabel.textAlignment = NSTextAlignmentCenter;

    [self.view addSubview:productLabel];

    
  
    
    }

}
-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
       
    }
    else{
        [Language setLanguage:@"en"];

    }
    
    
}
-(void)performActionOnPassword
{
    ProductLoginViewController *objProductViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductLoginViewController"];
    [self.navigationController pushViewController:objProductViewController animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIBarButtonItem *)addRightBarButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 20, 15)];
    //        [button setTitle:@"Share" forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"feedback"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(messageButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    return rightItem;
    
    
    
}
-(void)messageButtonClickedAction:(UIButton *)sender
{
    //    FeddbackViewController *objFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeddbackViewController"];
    //
    //    [self.navigationController pushViewController:objFeddbackViewController animated:YES];
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    
    
    [objMFMailcomposeVc setSubject:@""];
    
   // [objMFMailcomposeVc setToRecipients:[NSArray arrayWithObject:@""]];
    [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert"alter:@"Title not found"] message:[Language get:@"Please Signed in" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }
    
    
    
}


#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.tempArray.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



////// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//    // self.fetchedRecordsArray=[self getEntityCount];
//
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//        deleteIndexPath=indexPath;
//        [self showAlert];
//    }
//
//}
//
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    //    BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
    //
    //    if (fileExists)
    //    {
    //        cell.orderImageView.image=[UIImage imageWithContentsOfFile:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
    //    }
    //
    
    
    //Disable selection
    //  cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    NSData *data = [[[tempArray objectAtIndex:indexPath.row] valueForKey:@"quotetext"] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

    cell.textLabel.text= decodevalue;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSData *data = [[[tempArray objectAtIndex:indexPath.row] valueForKey:@"quotetext"] dataUsingEncoding:NSUTF8StringEncoding];
    temprature = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

    
    BarViewController *objBarViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BarViewController"];
    objBarViewController.temperatureId=[[tempArray objectAtIndex:indexPath.row] valueForKey:@"quoteid"];
    
    
    [self.navigationController pushViewController:objBarViewController animated:YES];
    
 
    
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%@",error);
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}



@end
