//
//  CalculatorCustomCell.m
//  PipeApp
//
//  Created by Redbytes on 28/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CalculatorCustomCell.h"

@implementation CalculatorCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       

        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.pipeDimentionResultLabel.layer setCornerRadius:5.0];
    [self.pressureLossResultLabel.layer setCornerRadius:5.0];
    [self.pressureDropResultLabel.layer setCornerRadius:5.0];
    [self.pipeTypeResultLabel.layer setCornerRadius:5.0];

    [self.velocityResultLabel.layer setCornerRadius:5.0];
    [self.calculateButton.layer setCornerRadius:5.0];
    [self.clearAllButton.layer setCornerRadius:5.0];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)calculateButtonClicked:(id)sender {
}

- (IBAction)clearAllButtonClicked:(id)sender {
}
@end
