//
//  AppDelegate.h
//  PipeApp
//
//  Created by Redbytes on 25/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashBoardViewController.h"
#import "ProductViewController.h"
#import "DocumentViewController.h"
#import "CalculatorViewController.h"
#import "SettingViewController.h"
#import "AboutViewController.h"
#import "LoginViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CalcLoginViewController.h"
#import "ProductLoginViewController.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}

@property (strong, nonatomic) MPMoviePlayerController *streamPlayer;
@property  MPMoviePlaybackState playerState;

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)UITabBarController *objTab;

@property(strong ,nonatomic)UINavigationController *objNvcDashboard;
@property(strong ,nonatomic)UINavigationController *objNvcProduct;
@property(strong ,nonatomic)UINavigationController *objNvcDocument;
@property(strong ,nonatomic)UINavigationController *objNvcCalculator;
@property(strong ,nonatomic)UINavigationController *objNvcSetting;
@property(strong ,nonatomic)UINavigationController *objNvcAbout;

@property(strong,nonatomic)DashBoardViewController *objDashBoardViewController;
@property(strong,nonatomic)ProductViewController *objProductViewController;
@property(strong,nonatomic)DocumentViewController *objDocumentViewController;
@property(strong,nonatomic)CalculatorViewController *objCalculatorViewController;
@property(strong,nonatomic)SettingViewController *objSettingViewController;
@property(strong,nonatomic)AboutViewController *objAboutViewController;
@property(strong,nonatomic)LoginViewController *objLoginViewController;

//sqlite
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)makeTabBar;

@end
