//
//  ConcentraionDetail.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GlykolConcentration;

@interface ConcentraionDetail : NSManagedObject

@property (nonatomic, retain) NSNumber * concentrationValue;
@property (nonatomic, retain) NSNumber * temrature;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) GlykolConcentration *glykolConcentration;

@end
