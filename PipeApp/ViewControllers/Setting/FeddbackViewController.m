//
//  FeddbackViewController.m
//  PipeApp
//
//  Created by Redbytes on 01/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "FeddbackViewController.h"

@interface FeddbackViewController ()

@end

@implementation FeddbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.sendButton.layer setCornerRadius:5.0];
    
//    [[self.remarkTextView layer] setBorderColor:[[UIColor grayColor] CGColor]];
//    [[self.remarkTextView layer] setBorderWidth:2.3];
//    [[self.remarkTextView layer] setCornerRadius:15];
//    
    [self.remarkTextView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [self.remarkTextView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.remarkTextView.layer setBorderWidth: 0.2];
    [self.remarkTextView.layer setCornerRadius:8.0f];
    [self.remarkTextView.layer setMasksToBounds:YES];
    
    self.nemeTextfield.text= self.docRecordName;
    

}
-(void)viewWillAppear:(BOOL)animated
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    // NSString * selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([savedValue isEqualToString:@"English"])
    {
               [Language setLanguage:@"en"];
        
    }
    else
    {
        
        [Language setLanguage:@"de"];
        
    }
    NSLog(@"%@",[Language get:@"Remark" alter:@"Title not found"]);
     NSLog(@"%@",[Language get:@"Name" alter:@"Title not found"]);
     NSLog(@"%@",[Language get:@"E-Mail" alter:@"Title not found"]);
    self.defaultLabel.text=[Language get:@"Remark" alter:@"Title not found"];
    self.nemeTextfield.placeholder=[Language get:@"Name" alter:@"Title not found"];
   

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendButtonClicked:(id)sender
{
    
    
    if([self.nemeTextfield.text isEqualToString:@""])
    {
        [self showAlert];
    }
    else if([self.emailTextfield.text isEqualToString:@""] || ![self validateEmailWithString:self.emailTextfield.text])
    {
        [self showAlert];
    }
    else if([self.remarkTextView.text isEqualToString:@""])
    {
        [self showAlert];
    }

    else
    {
        MFMailComposeViewController *objMFMailComposeVc = [[MFMailComposeViewController alloc]init];
       // objMFMailComposeViewController.mailComposeDelegate =self
        objMFMailComposeVc.mailComposeDelegate = self;
        
        NSString *body = [NSString stringWithFormat:@"%@",self.remarkTextView.text];
        
        NSString *subject = [NSString stringWithFormat:@"%@",self.nemeTextfield.text];
        
        NSString *to = [NSString stringWithFormat:@"%@",self.emailTextfield.text];
        [objMFMailComposeVc setSubject:subject];
        [objMFMailComposeVc setMessageBody:body isHTML:YES];
        [objMFMailComposeVc setToRecipients:[NSArray arrayWithObject:to]];
        if ([MFMailComposeViewController canSendMail])
        {
            [self presentViewController:objMFMailComposeVc animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get: @"Please Signed in" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
            
            //alert.delegate=self;
            [alert show];
        }

        
//        [self.view endEditing:YES];
//        [self popViewController];
        
    }
 
    
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%@",error);
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert
{
    [[[UIAlertView alloc] initWithTitle:@"Fehler im Formular" message:@"Füllen Sie die Pflichtfelder aus und geben Sie gültige Emailadressen an." delegate:nil cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil] show];
}

#pragma mark UITextView delegates

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
    CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    
    if(self.remarkTextView==textView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (scale == 2.0f)
            {
                if (pixelHeight == 960.0f)
                {
                    [self.scrollView setContentOffset:CGPointMake(0,50) animated:YES];
                    
                }
                
                else if (pixelHeight == 1136.0f)
                {
                    [self.scrollView setContentOffset:CGPointMake(0,50) animated:YES];
                    
                }
                
            }
            
        }
        
    }
     self.defaultLabel.hidden = YES;
    //[self.scrollView setContentOffset:CGPointMake(0,100) animated:YES];
}

- (void)textViewDidChange:(UITextView *)txtView
{
    self.defaultLabel.hidden = ([txtView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView
{
    self.defaultLabel.hidden = ([txtView.text length] > 0);
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound )
    {
        
        return YES;
        
    }
    [txtView resignFirstResponder];
    //[self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    return NO;
}
#pragma mark UITextFeild delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
