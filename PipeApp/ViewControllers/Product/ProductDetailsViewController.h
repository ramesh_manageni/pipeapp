//
//  ProductDetailsViewController.h
//  PipeApp
//
//  Created by Redbytes on 21/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<MessageUI/MessageUI.h>

@interface ProductDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *productNameView;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property(nonatomic,strong)NSString *productName;

@property (strong, nonatomic) IBOutlet UILabel *productDetailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UILabel *applicationLabel;
//@property (strong, nonatomic) UITableView *photosTableView;
@end
