//
//  DocumentViewController.m
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DocumentViewController.h"
#import "DocumentCustomCell.h"
#import "NavigationUtility.h"
#import "Language.h"

@interface DocumentViewController ()
{
     NSIndexPath *deleteIndexPath;
}

@property (nonatomic,strong)NSMutableArray* fetchedRecordsArray;

@end

@implementation DocumentViewController

@synthesize fetchedRecordsArray,addDocumentbutton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    self.addDocumentbutton.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];
    
    

}
-(void)linkButtonCliclked
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/ch/app/meine-messungen-my-measures/id628109928?mt=8"]];//https://itunes.apple.com/ch/app/meine-messungen-my-measures/id628109928?mt=8
   // http://www.google.com
}
-(void)viewWillAppear:(BOOL)animated
{
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    if (![password isEqualToString:@"Bru99"]) {
        [self performActionOnPassword];
    }
    else
    {

    [self.DocTableView setEditing:NO];

    self.navigationItem.hidesBackButton = YES;
    
    [self.tabBarController.tabBar setHidden:NO];
   
    
    self.fetchedRecordsArray=[self getEntityCount];
    [self.DocTableView reloadData];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"]) {
        
        
        
        [Language setLanguage:@"en"];
        [self localizationAction];
        
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
//                                                  forKey:@"AppleLanguages"];
       // [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        
        
    }
    else
    {
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"de", nil] forKey:@"AppleLanguages"];
//        
//        
//        
//        
//       [[NSUserDefaults standardUserDefaults] synchronize];
       
        [Language setLanguage:@"de"];
       
        //self.navigationItem.title=
        [self localizationAction];
        
    }
        self.navigationItem.title = [Language get:@"Documents" alter:@"title not found"];
    [self addLinkLabel];
        
    }
    
}
-(void)performActionOnPassword
{
    LoginViewController *objLoginViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objLoginViewController animated:NO];
}
#pragma mark actions
-(UIBarButtonItem *)addRightBarButton
{
    
    UIBarButtonItem *barButtonItem;
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"Deutsch"])
    {
        [Language setLanguage:@"de"];
    barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:[Language get:@"Edit" alter:@"title not found"] withImage:nil];
    }
    
    else
    {
    barButtonItem =[NavigationUtility createRightBarButtonForViewController:self withTitle:@"Edit" withImage:nil];
    }
    [[barButtonItem.customView.subviews objectAtIndex:0] setTitleColor:[UIColor colorWithRed:61/255.0 green:126/255.0 blue:254/255.0 alpha:1] forState:UIControlStateNormal];
    [[barButtonItem.customView.subviews objectAtIndex:0] addTarget:self action:@selector(toggleEdit:) forControlEvents:UIControlEventTouchUpInside];
    return barButtonItem;
}
-(void)toggleEdit:(UIButton *)button
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    //[self.DocTableView setEditing:YES];
    [self.DocTableView reloadData];
    
    [self.DocTableView setEditing:!self.DocTableView.editing animated:YES];
    
    if (self.DocTableView.editing)
    {
        if ([savedValue isEqualToString:@"Deutsch"]) {
            [Language setLanguage:@"de"];
            [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:[Language get:@"Done" alter:@"title not found"] forState:UIControlStateNormal];
        }
        else
        {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"Done" forState:UIControlStateNormal];
        }
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(30, 3, 70, 30)];
        
    
    }
    
    else
    {
        
        
        if ([savedValue isEqualToString:@"Deutsch"]) {
            [Language setLanguage:@"de"];
            [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:[Language get:@"Edit" alter:@"title not found"] forState:UIControlStateNormal];
            
        }
        else
        {
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setTitle:@"Edit" forState:UIControlStateNormal];
        }
        [[self.navigationItem.rightBarButtonItem.customView.subviews objectAtIndex:0] setFrame:CGRectMake(10, 3, 95, 30)];
        
    }

}
- (IBAction)addDocButtonClicked:(id)sender
{
    DocumentDetailViewController *objDocumentDetailViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DocumentDetailViewController"];
    
    [self.navigationController pushViewController:objDocumentDetailViewController animated:YES];
    
}

-(NSMutableArray *)getEntityCount
{
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"Document" inManagedObjectContext:context]];
    NSError *error;
    
    return [[[NSArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]] mutableCopy];
}

-(void)localizationAction
{
    self.navigationItem.title=[Language get:@"Documents" alter:@"title not found"];
   NSString *temp = [Language get:@"+ Add Documentation" alter:@"title not found"];
   [self.addDocumentbutton setTitle:temp forState:UIControlStateNormal];
    if ([self.fetchedRecordsArray count]!=0) {
        self.navigationItem.rightBarButtonItem = [self addRightBarButton];
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.fetchedRecordsArray.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        deleteIndexPath=indexPath;
        UIAlertView *alret = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to delete"delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",@"Cancel", nil];
    [alret show];
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        
        if(buttonIndex == 0)//OK button pressed
        {
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context = [delegate managedObjectContext];
            
            //    1
            [self.DocTableView beginUpdates];
            // Delete the row from the data source
            
            //    2
            [self.DocTableView deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            //    3
            [context deleteObject:[self.fetchedRecordsArray objectAtIndex:deleteIndexPath.row]];
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
            //    4
            
            self.fetchedRecordsArray=[self  getEntityCount];
            
//            if(self.fetchedRecordsArray.count==0)
//            {
//                [button removeFromSuperview];
//                
//                [self.tableviewObject addSubview:button];
//                
//            }
//            else
//            {
//                [button removeFromSuperview];
//            }
//            
//            if(self.fetchedRecordsArray.count==0)
//            {
//                self.navigationItem.rightBarButtonItem = nil;
//                [self.tableviewObject setEditing:NO];
//            }
            
            [self.DocTableView endUpdates];
            [self.DocTableView reloadData];
            if ([self.fetchedRecordsArray count]==0)
            {
                [self.DocTableView setEditing:NO];
                self.navigationItem.rightBarButtonItem = nil;
            }
            
            
        }

    }
}



////// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    // self.fetchedRecordsArray=[self getEntityCount];
//    
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//        deleteIndexPath=indexPath;
//        [self showAlert];
//    }
//    
//}
//
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    DocumentCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"DocumentCustomCell" bundle:nil ] forCellReuseIdentifier:CellIdentifier];
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
//    BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
//    
//    if (fileExists)
//    {
//        cell.orderImageView.image=[UIImage imageWithContentsOfFile:[[self.fetchedRecordsArray objectAtIndex:indexPath.row] imageURL]];
//    }
//    
    
    
    //Disable selection
  //  cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.DocNameLabel.text=[[self.fetchedRecordsArray objectAtIndex:indexPath.row] docName];
    cell.DocTimeLabel.text  = [[self.fetchedRecordsArray objectAtIndex:indexPath.row] docTime];
    cell.DocDateLabel.text =[[self.fetchedRecordsArray objectAtIndex:indexPath.row] docDate];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   ShowDocumentViewController *objShowDocumentViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ShowDocumentViewController"];
    
    objShowDocumentViewController.docRecord=[self.fetchedRecordsArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:objShowDocumentViewController animated:NO];
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) ];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) ];
}

-(void)addLinkLabel
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 480){
                // iPhone retina-3.5 inch CGRectMake(20, 350, 227, 55)
                UIButton *LinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [LinkButton setFrame:CGRectMake(0, 385, 320, 50)];
                [LinkButton  setTitle:[Language get:@"My measure and Dimensions" alter:@"Tilte not found"]forState:UIControlStateNormal];//Meine Maßnahme und Abmessungen
                [LinkButton setTitleColor:[UIColor colorWithRed:49/255.0 green:131/255.0 blue:254/255.0 alpha:1] forState:UIControlStateNormal];
                LinkButton.titleLabel.font = [UIFont systemFontOfSize:12];
                [LinkButton addTarget:self action:@selector(linkButtonCliclked) forControlEvents:UIControlEventTouchUpInside];
                
                LinkButton.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];
                [self.view addSubview:LinkButton];

            }
            
            
            else{
                // iPhone retina-4 inch
                
                UIButton *LinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [LinkButton setFrame:CGRectMake(0, self.DocTableView.frame.size.height+self.DocTableView.frame.origin.y, self.view.frame.size.width, 43)];
                [LinkButton  setTitle:[Language get:@"My measure and Dimensions" alter:@"Tilte not found"]forState:UIControlStateNormal];
                [LinkButton setTitleColor:[UIColor colorWithRed:49/255.0 green:131/255.0 blue:254/255.0 alpha:1] forState:UIControlStateNormal];
                LinkButton.titleLabel.font = [UIFont systemFontOfSize:12];
                [LinkButton addTarget:self action:@selector(linkButtonCliclked) forControlEvents:UIControlEventTouchUpInside];
                
                LinkButton.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];
                [self.view addSubview:LinkButton];

                
                }
        
        }
        
        
        else {
            // not retina display
        }
    }
}

@end
