//
//  Document.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 12/30/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Document : NSManagedObject

@property (nonatomic, retain) NSString * docDate;
@property (nonatomic, retain) NSString * docImageName;
@property (nonatomic, retain) NSString * docName;
@property (nonatomic, retain) NSString * docRecordingName;
@property (nonatomic, retain) NSString * docTime;

@end
