//
//  languageViewController.m
//  PipeApp
//
//  Created by Redbytes on 30/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "languageViewController.h"

@interface languageViewController ()
{
     NSString *valueToSave;
}

@property(strong,nonatomic)NSMutableArray *languageArray;
@property(strong,nonatomic)NSMutableArray *unitArray;
@property(strong,nonatomic)NSMutableArray *finalArray;
@property(strong,nonatomic) NSString *savedValue;

@end

@implementation languageViewController

@synthesize languageArray,unitArray,isLanguageSelected,isUnitSelected,finalArray,savedValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
   
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    savedValue = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"English"])
    {
        [Language setLanguage:@"en"];
    }
    else
    {
        [Language setLanguage:@"de"];
    }
    self.languageArray=[[NSMutableArray alloc] initWithObjects:@"English",@"Deutsch",nil];
    self.unitArray=[[NSMutableArray alloc] initWithObjects:[Language get:@"Metric"  alter:@"Title not found"],@"US",nil];

    
    if(isLanguageSelected)
    {
        self.navigationItem.title =[Language get:@"Language"  alter:@"Title not found"];// @"Language";
        self.finalArray=self.languageArray;
        
        NSLog(@"Final Array %@",self.finalArray);
    }
    if(isUnitSelected)
    {
        self.navigationItem.title = [Language get:@"Unit"  alter:@"Title not found"];//@"Unit";
        self.finalArray=self.unitArray;
        NSLog(@"Final Array %@",self.finalArray);
        
    }

}

-(void)viewDidDisappear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.finalArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(isLanguageSelected)
    {
        savedValue=[[NSUserDefaults standardUserDefaults]
                    stringForKey:@"languageName"];
    }
    if(isUnitSelected)
    {
        savedValue=[[NSUserDefaults standardUserDefaults]
                    stringForKey:@"unitName"];
    }

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil )
    {
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    if ([savedValue isEqualToString:[finalArray objectAtIndex:indexPath.row]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.text=[self.finalArray objectAtIndex:indexPath.row];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.00001;
}
#pragma mark UITableView Delegate

// UITableView Delegate Method
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    
//     NSString *valueToSave =[finalArray objectAtIndex:indexPath.row];
//    if( isLanguageSelected)
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"languageName"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//    if(isUnitSelected)
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"unitName"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//    
//    [tableView reloadData];

    valueToSave =[finalArray objectAtIndex:indexPath.row];
    if( isLanguageSelected)
    {
        savedValue = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"languageName"];
        if ([savedValue isEqualToString:@"English"])
        {
            [Language setLanguage:@"en"];
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:@"Do you want to change language"  delegate:self cancelButtonTitle:[Language get: @"Cancel" alter:@"Title not found"] otherButtonTitles:[Language get: @"Ok" alter:@"Title not found"], nil];
            [alert show];
        }
        else
        {
            [Language setLanguage:@"de"];
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"language" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Cancel" alter:@"Title not found"] otherButtonTitles:[Language get: @"Ok" alter:@"Title not found"], nil];
            [alert show];
        }

        
        
        
    }
    if(isUnitSelected)
    {
        if (indexPath.row == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"unitName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    [tableView reloadData];


}
//-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
//}

#pragma mark:Alert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    AppDelegate *objApp = [UIApplication sharedApplication].delegate;
    

    //[app performSelector:@selector(suspend)];
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"languageName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([valueToSave isEqualToString:@"English"]) {
                // [Language setLanguage:@"en"];unitName
                //"Metric"="metrisch";
                [[NSUserDefaults standardUserDefaults] setObject:@"Metric" forKey:@"unitName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                            valueForKey:@"AppleLanguages"]);
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
                                                          forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                            valueForKey:@"AppleLanguages"]);
                
                [Language setLanguage:@"en"];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"metrisch" forKey:@"unitName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"de", nil] forKey:@"AppleLanguages"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                            valueForKey:@"AppleLanguages"]);
                [Language setLanguage:@"en"];
            }
            
            //[app performSelector:@selector(suspend)];
            //[app performSelector:@selector(terminateWithSuccess)];
                         //exit(0);
            
            [self.languageTableview reloadData];
            [objApp makeTabBar];
            
        default:
            break;
    }
}
@end
