//
//  GlykolConcentration.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ConcentraionDetail;

@interface GlykolConcentration : NSManagedObject

@property (nonatomic, retain) NSNumber * concentration;
@property (nonatomic, retain) NSSet *concentraionDetail;
@end

@interface GlykolConcentration (CoreDataGeneratedAccessors)

- (void)addConcentraionDetailObject:(ConcentraionDetail *)value;
- (void)removeConcentraionDetailObject:(ConcentraionDetail *)value;
- (void)addConcentraionDetail:(NSSet *)values;
- (void)removeConcentraionDetail:(NSSet *)values;

@end
