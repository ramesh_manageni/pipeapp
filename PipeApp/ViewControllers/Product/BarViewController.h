//
//  BarViewController.h
//  PipeApp
//
//  Created by Redbytes on 17/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKSTableView.h"
#import "ProductListViewController.h"


@interface BarViewController : UIViewController<SKSTableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *productLabel;

@property (nonatomic, weak) IBOutlet SKSTableView *tableView;
@property(nonatomic,strong) NSString *temperatureId;
@end
