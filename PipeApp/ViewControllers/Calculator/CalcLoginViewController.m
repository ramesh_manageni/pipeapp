//
//  CalcLoginViewController.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 1/28/15.
//  Copyright (c) 2015 Redbytes. All rights reserved.
//

#import "CalcLoginViewController.h"

@interface CalcLoginViewController ()

@end

@implementation CalcLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.navigationItem.title = @"Login";
    [self.loginButton.layer setCornerRadius:5.0];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{    self.navigationItem.hidesBackButton = YES;

    [self.tabBarController.tabBar setHidden:NO];
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    if ([password isEqualToString:@"Bru99"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginButtonClicked:(id)sender
{
    if ([self.passworTextField.text isEqualToString:@"Bru99"] ) {
        
        [[NSUserDefaults standardUserDefaults] setObject:self.passworTextField.text forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        permission = YES;
        [self.navigationController popToRootViewControllerAnimated:NO];
        self.passworTextField.text = @"";
    }

    else
    {
        permission = NO;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:[Language get:@"Please enter Correct Password" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        [alert show];
        self.passworTextField.text =@"";
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return YES;
    
}
@end
