//
//  BarViewController.m
//  PipeApp
//
//  Created by Redbytes on 17/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "BarViewController.h"
#import "DBManager.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"


@interface BarViewController ()

@property(nonatomic,strong)NSArray *barArray;
@property (nonatomic, strong) NSArray *contents;
@property (nonatomic, strong) NSArray *dimentionArray;



@end

@implementation BarViewController

@synthesize barArray,temperatureId,dimentionArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%@",temperatureId);
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title =[Language get:@"Bar" alter:@"Title not found"];
    self.tableView.SKSTableViewDelegate = self;
    self.productLabel.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];


    self.barArray=[[DBManager getSharedInstance] getMaxBarValueWithId:temperatureId];
    self.dimentionArray=[[DBManager getSharedInstance] getMaxDimentionWithId:temperatureId];
    _contents=[self getContents];

}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:NO];

    self.productLabel.text = temprature;
    [self.productLabel setHidden:YES];
    
    // for product label
    UILabel *productLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, self.view.frame.size.width, 35)];
    productLabel.backgroundColor = [UIColor whiteColor];
    productLabel.text = temprature;
    
    productLabel.layer.borderColor = [UIColor blackColor].CGColor;
    productLabel.layer.borderWidth = 1.0;
    productLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:productLabel];
    ///////////////

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)getContents
{
    NSMutableArray *mainArray=[[NSMutableArray alloc] init];
    NSMutableArray *superArray=[[NSMutableArray alloc] init];
    int count=1;

    for(int i=0;i<self.barArray.count;i++)
    {
        NSMutableArray *subArray=[[NSMutableArray alloc] init];

        [subArray addObject:[[self.barArray objectAtIndex:i] valueForKey:@"quotetext"]];
        
        

        for(int j=0;j<self.dimentionArray.count;j++)
        {
            
            if([[[self.dimentionArray objectAtIndex:j] valueForKey:@"quoteid"] integerValue]%10 == count)
            {
            [subArray addObject:[[self.dimentionArray objectAtIndex:j] valueForKey:@"quotetext"]];
            }
        }

        count++;
        
        [mainArray addObject:subArray];
        
        subArray=nil;
    }
    
    
    [superArray addObject:mainArray];
    
    NSLog(@"%@",superArray);

   
//    if (!_contents)
//    {
//        _contents = @[
//                      @[
//                          @[@"Section0_Row0", @"Row0_Subrow1",@"Row0_Subrow2"],
//                          @[@"Section0_Row1", @"Row1_Subrow1", @"Row1_Subrow2", @"Row1_Subrow3", @"Row1_Subrow4", @"Row1_Subrow5", @"Row1_Subrow6", @"Row1_Subrow7", @"Row1_Subrow8", @"Row1_Subrow9", @"Row1_Subrow10", @"Row1_Subrow11", @"Row1_Subrow12"],
//                          @[@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2"]],
//                  
//                      ];
//    }
    
    return superArray;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"[self.contents count]=%lu",(unsigned long)[self.contents count]);
    return [self.contents count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"[self.contents count]=%lu",(unsigned long)[self.contents[section] count]);

    return [self.contents[section] count];
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.contents[indexPath.section][indexPath.row] count] - 1;
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"section=%ld",(long)indexPath.section);
      NSLog(@"row=%ld",(long)indexPath.row);
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        
        return YES;
        
    }
    
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSData *data = [self.contents[indexPath.section][indexPath.row][0] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

    
    cell.textLabel.text = decodevalue;
    
//    if ((indexPath.section == 0 && (indexPath.row == 1 || indexPath.row == 0)) || (indexPath.section == 1 && (indexPath.row == 0 || indexPath.row == 2)))
//        cell.expandable = YES;
//    else
        cell.expandable = YES;
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NSData *data = [self.contents[indexPath.section][indexPath.row][indexPath.subRow] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", decodevalue];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    NSData *data = [self.contents[indexPath.section][indexPath.row][indexPath.subRow] dataUsingEncoding:NSUTF8StringEncoding];
    bar = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    NSLog(@"Bar = %@",bar);
     NSLog(@"temprature = %@",temprature);
    
    
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSData *data = [self.contents[indexPath.section][indexPath.row][indexPath.subRow] dataUsingEncoding:NSUTF8StringEncoding];
    dimension = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    NSLog(@"Dimension = %@",dimension);
    NSLog(@"Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    
    
    self.dimentionArray=[[DBManager getSharedInstance] getMaxDimentionWithId:temperatureId];
    NSLog(@"Product Images %@",[[DBManager getSharedInstance] getProductImageWithTempId:temperatureId barId:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1] dimensionId:[NSString stringWithFormat:@"%ld",(long)indexPath.subRow]]);
    
    ProductListViewController *objProductListViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductListViewController"];
    
    objProductListViewController.productImageArray =[[DBManager getSharedInstance] getProductImageWithTempId:temperatureId barId:[NSString stringWithFormat:@"%ld",(long)indexPath.row+1] dimensionId:[NSString stringWithFormat:@"%ld",(long)indexPath.subRow]];
    
    [self.navigationController pushViewController:objProductListViewController animated:YES];
    

}
#pragma mark tableviewdelegate 
-(CGFloat)tableView:(SKSTableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.01;
}

-(CGFloat)tableView:(SKSTableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(UIView*)tableView:(SKSTableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];//CGRectMake(0, 0, 0, 0)
}

-(UIView*)tableView:(SKSTableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Actions




- (void)reloadTableViewWithData:(NSArray *)array
{
    self.contents = array;
    
    // Refresh data not scrolling
    //    [self.tableView refreshData];
    
    [self.tableView refreshDataWithScrollingToIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
}




@end
