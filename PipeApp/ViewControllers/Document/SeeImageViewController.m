//
//  SeeImageViewController.m
//  temp
//
//  Created by Shaikh Rizwan on 11/25/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import "SeeImageViewController.h"
#import "SettingViewController.h"
#import "UIImage+ScreenShots.h"

@interface SeeImageViewController ()
{
    UIImageView *imgView;
    UILabel *latLabel,*longLabel;
     
}
@property (nonatomic, strong)	NSArray			*colorSchemes;
@property (nonatomic, strong)	NSDictionary	*contents;
@property (nonatomic, strong)	id				currentPopTipViewTarget;
@property (nonatomic, strong)	NSMutableArray	*visiblePopTipViews;
@property (nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation SeeImageViewController
@synthesize locationManager=_locationManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
}

- (void)viewWillAppear:(BOOL)animated
{
    imgView=nil;
    latLabel=nil;
    longLabel=nil;
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
    
    imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 120, 300, 300)];//CGRectMake(50, 120, 250, 250)
    [imgView setUserInteractionEnabled:YES];
    [self.view addSubview:imgView];
    imgView.image = DocumentImage;
    
    for (int i=0; i<[self.fetchArray count]; i++) {
        CGFloat x=[[[self.fetchArray objectAtIndex:i] valueForKey:@"x"] floatValue];
        CGFloat y=[[[self.fetchArray objectAtIndex:i] valueForKey:@"y"] floatValue];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(x, y, 5, 5)];
        button.backgroundColor=[UIColor blackColor];
        button.tag=[[[self.fetchArray objectAtIndex:i] valueForKey:@"tag"] intValue];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [imgView addSubview:button];
        UILabel *textLabel=   [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
        textLabel.font=[UIFont systemFontOfSize:12];
        [self toolTipButtonAction:button TextLabel:textLabel];

        
       // DocumentImage = [self toolTipsScreenShots];
    }
    
    CLLocation *currentLocation = [self.locationArray objectAtIndex:0];

   latLabel = [[UILabel alloc]initWithFrame:CGRectMake(210, 250, 90, 20)];//CGRectMake(10, imgView.frame.size.height+140, 80, 25)
       latLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    
    [imgView addSubview:latLabel];
   longLabel = [[UILabel alloc]initWithFrame:CGRectMake(210, 275, 90, 20)];//CGRectMake(150, imgView.frame.size.height+140, 80, 25)];
     longLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    
    [imgView addSubview:longLabel];
    DocumentImage = [self toolTipsScreenShots];

}
-(void)buttonAction:(UIButton *)sender
{
    UILabel *textLabel=   [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
    [self toolTipButtonAction:sender TextLabel:textLabel];

    }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.locationManager =nil;
}

- (void)toolTipButtonAction:(UIButton *)sender TextLabel:(UILabel *)textLabel
{
    [self dismissAllPopTipViews];
	
	if (sender == self.currentPopTipViewTarget) {
		// Dismiss the popTipView and that is all
		self.currentPopTipViewTarget = nil;
	}
    
    
    
    else {
        
        NSString *contentMessage = nil;
        UIView *contentView = nil;
		//NSNumber *key = [NSNumber numberWithInteger:[(UIView *)sender tag]];
        
        
		
        
        
        for (int i=0; i<[self.fetchArray count]; i++) {
            
            if (sender.tag ==[[[self.fetchArray objectAtIndex:i] valueForKey:@"tag"] intValue])
            {
                textLabel.text=[[self.fetchArray objectAtIndex:i] valueForKey:@"textFieldText"];
            }
            
            
        }

        id content = textLabel;
        
		if ([content isKindOfClass:[UIView class]]) {
			contentView = content;
		}
        
        else
        {
			contentMessage = content;
        }
		
        
        //
        //		NSArray *colorScheme = [self.colorSchemes objectAtIndex:foo4random()*[self.colorSchemes count]];
        //		UIColor *backgroundColor = [colorScheme objectAtIndex:0];
        //		UIColor *textColor = [colorScheme objectAtIndex:1];
		
        
		CMPopTipView *popTipView;
//        
//        UIButton *crossbutton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [crossbutton setFrame:CGRectMake(205,13, 15,15)];
//        // button.backgroundColor=[UIColor redColor];
//        [crossbutton setBackgroundImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
//        
//        [crossbutton addTarget:self action:@selector(crossButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
		
        if (contentView) {
			popTipView = [[CMPopTipView alloc] initWithCustomView:contentView];
            //[popTipView addSubview:crossbutton];
            
            
            
		}
        else{
            
            
			popTipView = [[CMPopTipView alloc] initWithMessage:contentMessage];
        }
		
		//popTipView.delegate1 = self;
        popTipView.delegate=self;
		popTipView.backgroundColor=[UIColor clearColor];
        
        popTipView.animation = arc4random() % 2;
		popTipView.has3DStyle = (BOOL)(arc4random() % 2);
		
		popTipView.dismissTapAnywhere = YES;
        //[popTipView autoDismissAnimated:YES atTimeInterval:8.0];
        
		if ([sender isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)sender;
			[popTipView presentPointingAtView:button inView:imgView animated:YES];
		}
		
		
		[self.visiblePopTipViews addObject:popTipView];
		self.currentPopTipViewTarget = sender;
	}
    
    
    
}
- (void)dismissAllPopTipViews
{
	while ([self.visiblePopTipViews count] > 0) {
		CMPopTipView *popTipView = [self.visiblePopTipViews objectAtIndex:0];
		[popTipView dismissAnimated:YES];
		[self.visiblePopTipViews removeObjectAtIndex:0];
	}
}
#pragma mark - CMPopTipViewDelegate methods

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
	[self.visiblePopTipViews removeObject:popTipView];
	self.currentPopTipViewTarget = nil;
}

#pragma mark - UIViewController methods

- (void)willAnimateRotationToInterfaceOrientation:(__unused UIInterfaceOrientation)toInterfaceOrientation duration:(__unused NSTimeInterval)duration
{
	for (CMPopTipView *popTipView in self.visiblePopTipViews) {
		id targetObject = popTipView.targetObject;
		[popTipView dismissAnimated:NO];
		
		if ([targetObject isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)targetObject;
			[popTipView presentPointingAtView:button inView:imgView animated:NO];
		}
		else {
			UIBarButtonItem *barButtonItem = (UIBarButtonItem *)targetObject;
			[popTipView presentPointingAtBarButtonItem:barButtonItem animated:NO];
		}
	}
}

-(void)crossButtonAction:(UIButton *)sender
{
    CMPopTipView *obj = (CMPopTipView *)[sender superview];
    [obj dismissAnimated:YES];
   
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UIImage *)toolTipsScreenShots
{
    UIImage *viewImage = [UIImage imageFromView:imgView];
    return viewImage;
}

//- (IBAction)screenshotAction:(id)sender
//{
//[[NSOperationQueue mainQueue] addOperationWithBlock:^{
//    [self openPhotoPicker:sourceType];
//}];
//    self.screenshotImageview.image = [self toolTipsScreenShots];
//}
- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Current Location
    CLLocation *currentLocation = [locations objectAtIndex:0];
    //currentLocation.coordinate.latitude=47.574435;
    NSLog(@"currentLocation=%@",currentLocation);
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = currentLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    latLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    longLabel.text = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    //[self.mapView addAnnotation:point];
    
    
    
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location manager fails");
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation   *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"Location manager work");
}

@end
