//
//  AppDelegate.m
//  PipeApp
//
//  Created by Redbytes on 25/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AppDelegate.h"
#import "DBManager.h"

#import "DashBoardViewController.h"
#import "ProductViewController.h"
#import "DocumentViewController.h"
#import "CalculatorViewController.h"
#import "SettingViewController.h"
#import "AboutViewController.h"
#import "LoginViewController.h"
#import "PipeTypes.h"
#import "PipeVariableDetail.h"
#import "ConcentraionDetail.h"
#import "GlykolConcentration.h"
#import "Warter.h"
#import "WarterVariableDetail.h"
#import"PipeRoughness.h"


@implementation AppDelegate


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize objNvcDashboard,objNvcAbout,objNvcCalculator,objNvcDocument,objNvcProduct,objNvcSetting;

@synthesize objDashBoardViewController,objAboutViewController,objCalculatorViewController,objDocumentViewController,objProductViewController,objSettingViewController,objLoginViewController;

@synthesize playerState,streamPlayer;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setValueInUserDefaults];
    
    NSArray *tempArray=[[DBManager getSharedInstance] getMaxTemp];
    NSLog(@"Max temp = %@",tempArray);
    
    
    for(int i=0;i<tempArray.count;i++)
    {
//    NSData *data = [[[tempArray objectAtIndex:i] valueForKey:@"quotetext"] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//  NSString *decode = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//        
        
        
        NSData *data = [[[tempArray objectAtIndex:i] valueForKey:@"quotetext"] dataUsingEncoding:NSUTF8StringEncoding];
        NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        
        
    NSLog(@"Max temp = %@",decodevalue);
    }

    
       // Override point for customization after application launch.
    
    //save default language to database
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    

    
    
    if(savedValue==nil)
    {
      NSString *valueToSave = @"English";
     [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"languageName"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    }
 
    //save default Unit to database
    NSString *savedValueUnit = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"unitName"];
    if(savedValueUnit==nil)
    {
        NSString *valueToSave = @"Metric";
        [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"unitName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
    [self makeTabBar];
    [self setUpInitialData];

    return YES;
}
//For creating custom tabbar
-(void)makeTabBar
{
    [self localizationAction];
    _objTab=[[UITabBarController alloc] init];
   
    objDocumentViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DocumentViewController"];
    
    objDashBoardViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
    [objDashBoardViewController.tabBarItem setTitle:[Language get:@"Dashboard" alter:@"Title not found"]];
    [objDashBoardViewController.tabBarItem setImage:[UIImage imageNamed:@"Dashboard"]];
    
   // [_objDashBoardViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcDashboard=[[UINavigationController alloc]initWithRootViewController:objDashBoardViewController];
    objDashBoardViewController.tabBarItem.tag=0;
    
    objProductViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductViewController"];
    [objProductViewController.tabBarItem setTitle:[Language get:@"Products" alter:@"Title not found"]];
    [objProductViewController.tabBarItem setImage:[UIImage imageNamed:@"products"]];
    // [objProductViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcProduct=[[UINavigationController alloc]initWithRootViewController:objProductViewController];
    objProductViewController.tabBarItem.tag=1;
    
//    objDocumentViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DocumentViewController"];
//    [objDocumentViewController.tabBarItem setTitle:@"Documents"];
//    [objDocumentViewController.tabBarItem setImage:[UIImage imageNamed:@"documents.png"]];
    
    [objDocumentViewController.tabBarItem setTitle:[Language get:@"Documents" alter:@"Title not found"]];
    [objDocumentViewController.tabBarItem setImage:[UIImage imageNamed:@"documents"]];
    // [_objDashBoardViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcDocument=[[UINavigationController alloc]initWithRootViewController:objDocumentViewController];
    objDocumentViewController.tabBarItem.tag=2;
    
    objCalculatorViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CalculatorViewController"];
    [objCalculatorViewController.tabBarItem setTitle:[Language get:@"Calculator" alter:@"Title not found"]];
    [objCalculatorViewController.tabBarItem setImage:[UIImage imageNamed:@"calculator"]];
    // [_objDashBoardViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcCalculator=[[UINavigationController alloc]initWithRootViewController:objCalculatorViewController];
    objCalculatorViewController.tabBarItem.tag=3;

    objSettingViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [objSettingViewController.tabBarItem setTitle:[Language get:@"Setting" alter:@"Title not found"]];
    [objSettingViewController.tabBarItem setImage:[UIImage imageNamed:@"settingtab"]];
    // [_objDashBoardViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcSetting=[[UINavigationController alloc]initWithRootViewController:objSettingViewController];
    objSettingViewController.tabBarItem.tag=4;

    objAboutViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutViewController"];
    [objAboutViewController.tabBarItem setTitle:[Language get:@"About" alter:@"Title not found"]];
    [objAboutViewController.tabBarItem setImage:[UIImage imageNamed:@"aboutTab"]];
    // [_objDashBoardViewController.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"orderSelected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"order1.png"]];
    objNvcAbout=[[UINavigationController alloc]initWithRootViewController:objAboutViewController];
    objAboutViewController.tabBarItem.tag=5;

    _objTab.moreNavigationController.navigationBar.topItem.title = [Language get:@"More" alter:@"Title not found"];
    //_objTab.moreNavigationController.tabBarItem.title=[Language get:@"More" alter:@"Title not found"];
    
    
        NSArray* controllers = [NSArray arrayWithObjects:objNvcDashboard,objNvcCalculator,objNvcProduct,objNvcDocument,objNvcSetting,objNvcAbout,nil];
    [_objTab setViewControllers:controllers animated:NO];
    
    


    self.window.rootViewController=_objTab ;
    
    
//    UITabBarItem * tabBarItem =  [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"More", nil) image:nil tag:0];
//    [[_objTab.moreNavigationController.viewControllers objectAtIndex:0] setTabBarItem:tabBarItem];
//    [[_objTab.moreNavigationController.viewControllers objectAtIndex:0] setTitle:NSLocalizedString(@"More", nil)];
}
-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        
    }
    else{
        [Language setLanguage:@"en"];
        
    }
    
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//To save data to database
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

-(void)setValueInUserDefaults
{
   
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    if (password==nil) {
        
        password = @"";
        
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];

    }


}
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LocalDatabseModel" withExtension:@"mom"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LocalDatabseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)setUpInitialData
{
    
    if (![self isDataPresent])
    {
        
        // inserting pipe type and pipe type variable data
        
        NSArray *pipeTypesArray = @[@"Calpex Heizung",@"Calpex Sanitär",@"Eigerflex",@"Coolflex"];
        NSArray *pipeRoughness1 =@[@"0.007",@"0.01",@"0.01",@"0.01"];
        for (int i = 0; i < [pipeTypesArray count]; i++)
        {
            PipeTypes *pipeType = [NSEntityDescription insertNewObjectForEntityForName:@"PipeTypes" inManagedObjectContext:self.managedObjectContext];
            pipeType.pipeName = [pipeTypesArray objectAtIndex:i];
            
            NSString *csvFileName = [pipeTypesArray objectAtIndex:i];
            if ([csvFileName isEqualToString:@"Coolflex"]) {
                csvFileName = [pipeTypesArray objectAtIndex:2];
            }
            
            NSString* pipeTypeVariablePath = [[NSBundle mainBundle] pathForResource: csvFileName ofType: @"csv"];
            NSData* pipeTypeVariableData = [NSData dataWithContentsOfFile:pipeTypeVariablePath];
            NSString *pipeTypeVariableString = [[NSString alloc] initWithData:pipeTypeVariableData encoding:NSASCIIStringEncoding];
            NSArray *pipeTypeVariableLinesArray=[pipeTypeVariableString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\r\n"]];
            for (int i = 1;  i < pipeTypeVariableLinesArray.count; i++ )
            {
                NSString *pipeTypeVariableLine = [pipeTypeVariableLinesArray objectAtIndex:i];
                
                PipeVariableDetail *pipeTypeVariable=[NSEntityDescription insertNewObjectForEntityForName:@"PipeVariableDetail" inManagedObjectContext:self.managedObjectContext];
                
                NSArray *pipeTypeVariableDetails = [pipeTypeVariableLine componentsSeparatedByString:@","];
                
                pipeTypeVariable.pipetype = [pipeTypeVariableDetails objectAtIndex:0];
                pipeTypeVariable.pipeOuter = [NSNumber numberWithFloat:[[pipeTypeVariableDetails objectAtIndex:1] floatValue]];
                pipeTypeVariable.pipewallThickness = [NSNumber numberWithFloat:[[pipeTypeVariableDetails objectAtIndex:2] floatValue]];
                pipeTypeVariable.pipeInner = [NSNumber numberWithFloat:[[pipeTypeVariableDetails objectAtIndex:3] floatValue]];
                
                pipeTypeVariable.pipeType = pipeType;
                
                [pipeType addPipeVariableDetailObject:pipeTypeVariable];
                
            }
        
            PipeRoughness *objpipe=[NSEntityDescription insertNewObjectForEntityForName:@"PipeRoughness" inManagedObjectContext:self.managedObjectContext];
            
            objpipe.pipeName = [pipeTypesArray objectAtIndex:i];
            objpipe.surfaceRoughness =[NSNumber numberWithFloat:[[pipeRoughness1 objectAtIndex:i] floatValue]];
            
        }
        [self saveContext];
        
        
        //inserting concentartion data
        
        NSString* concentartionPath = [[NSBundle mainBundle] pathForResource:@"Database Glykol alloys" ofType: @"csv"];
        NSData* concentartionData = [NSData dataWithContentsOfFile:concentartionPath];
        NSString *concentartionString = [[NSString alloc] initWithData:concentartionData encoding:NSASCIIStringEncoding];
        NSArray *concentartionLinesArray=[concentartionString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\r\n"]];
        NSString *firtsLine = [ concentartionLinesArray objectAtIndex:0];
        NSArray *fisrtLineArray = [ firtsLine componentsSeparatedByString:@","];
        for (int i=1 ; i < [fisrtLineArray count]; i++)
        {
            if (i%2 != 0)
            {
                
                GlykolConcentration *concentrationType=[NSEntityDescription insertNewObjectForEntityForName:@"GlykolConcentration" inManagedObjectContext:self.managedObjectContext];
                concentrationType.concentration = [NSNumber numberWithInteger:[[fisrtLineArray objectAtIndex:i] integerValue]];
                
                for (int j=1 ; j < [concentartionLinesArray count]; j++)
                {
                    NSString *details = [concentartionLinesArray objectAtIndex:j];
                    NSArray *detailsArray = [details componentsSeparatedByString:@","];
                    ConcentraionDetail *concentrationDetails=[NSEntityDescription insertNewObjectForEntityForName:@"ConcentraionDetail" inManagedObjectContext:self.managedObjectContext];
                    concentrationDetails.temrature =[NSNumber numberWithInteger:[[detailsArray objectAtIndex:0] integerValue]];
                    
                    concentrationDetails.concentrationValue = [NSNumber numberWithFloat:[[detailsArray objectAtIndex:i] floatValue]];
                    concentrationDetails.value = [NSNumber numberWithFloat:[[detailsArray objectAtIndex:i+1] floatValue]];
                    concentrationDetails.glykolConcentration = concentrationType;
                    [concentrationType addConcentraionDetailObject:concentrationDetails];
                    
                    
                    
                }
                
                
                
            }
        }
        
        
        [self saveContext];
        
        // insert warter data details as per bars
        
        NSArray *barsArray = @[@"6"];
        for (NSString *bar in barsArray) {
            
            Warter *warter = [ NSEntityDescription insertNewObjectForEntityForName:@"Warter" inManagedObjectContext:[self managedObjectContext]];
            warter.tempInBar = bar;
            
            NSString* warterPath = [[NSBundle mainBundle] pathForResource:@"Database water" ofType: @"csv"];
            NSData* warterData = [NSData dataWithContentsOfFile:warterPath];
            NSString *warterString = [[NSString alloc] initWithData:warterData encoding:NSASCIIStringEncoding];
            NSArray *warterLinesArray=[warterString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\r\n"]];
            
            for (int i = 1; i < [warterLinesArray count]; i++) {
                
                NSString* warterDetails = [warterLinesArray objectAtIndex:i];
                NSArray *warterDetailsArray = [warterDetails componentsSeparatedByString:@","];
                
                WarterVariableDetail *warterVariableDetails =[NSEntityDescription insertNewObjectForEntityForName:@"WarterVariableDetail" inManagedObjectContext:[self managedObjectContext]];
                
                warterVariableDetails.temrature = [NSNumber numberWithInteger:[[warterDetailsArray objectAtIndex:0] integerValue]];
                warterVariableDetails.pressure = [NSNumber numberWithInteger:[[warterDetailsArray objectAtIndex:1] integerValue]];
                warterVariableDetails.density = [NSNumber numberWithFloat:[[warterDetailsArray objectAtIndex:2] floatValue]];
                warterVariableDetails.viscocity = [NSNumber numberWithFloat:[[warterDetailsArray objectAtIndex:3] floatValue]];
                warterVariableDetails.warter = warter;
                
                [warter addWarterVariableDetailObject:warterVariableDetails];
                
                
            }
            
        }
        
        [self saveContext];
        
    }
}

-(BOOL)isDataPresent
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    [fetchReq setEntity:[NSEntityDescription entityForName:@"PipeTypes" inManagedObjectContext:context]];
    NSError *error;
    NSArray *pipesArray = [[NSArray alloc]initWithArray:[context executeFetchRequest:fetchReq error:&error]];
    if (error)
    {
        NSLog(@"Appdelegate Error at fetching pipes type data : %@",error);
        return NO;
        
    }
    
    if (pipesArray.count > 0) {
        
        return YES;
        
    }
    
    return NO;
}




@end
