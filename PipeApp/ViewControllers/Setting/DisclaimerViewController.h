//
//  DisclaimerViewController.h
//  PipeApp
//
//  Created by Redbytes on 01/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisclaimerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *disclaimerTextView;

@end
