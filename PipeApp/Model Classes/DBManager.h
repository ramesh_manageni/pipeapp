//
//  DBManager.h
//  DhinchakApp
//
//  Created by apple on 11/11/13.
//  Copyright (c) 2013 redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DBManager : NSObject
{
    NSString *databasePath;
}

+(DBManager*)getSharedInstance;
-(BOOL)createDB;
- (NSArray*)getMaxTemp;
- (NSArray*)getMaxBarValueWithId:(NSString *)ID;
- (NSArray*)getMaxDimentionWithId:(NSString *)ID;
- (NSArray*)getProductImageWithTempId:(NSString *)tempId barId:(NSString *)barId dimensionId:(NSString *)dimensionId;


@end
