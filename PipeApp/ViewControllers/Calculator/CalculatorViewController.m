//
//  CalculatorViewController.m
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorCustomCell.h"
#import "PipeVariableDetail.h"
#import <math.h>
#import "GlykolConcentration.h"
#import "ConcentraionDetail.h"
#import "Warter.h"
#import "WarterVariableDetail.h"
#import "PipeTypes.h"
#import "PipeVariableDetail.h"
#import "PipeRoughness.h"



@interface CalculatorViewController ()
{
#pragma mark Rizwan
    
    float massFlowKgHr,massFlowKgSec,massFlowLitHr,waterDensity,volumeFRateLHr,volumeFRateLSec,dynamicViscosityMuekg,dynamicViscositykg;
    float kinematicViscosityMuem2,kinematicViscositym2,reynoldsNumber,pipeRoughness  ;
    
    float pressureLossPerMeter,pressureDrop;
    int lengthofPipe,powerOutput,WaterTemperature,maxPressureLoss,glykolConcent,volumeOrMassFlow,differenceWaterTemperature,index ;
    NSString *pipeType,*glykolString;
    
    NSMutableArray *pipeInner_m,*typeofPipeArray,*A_dm,*volumeFRateMSec,*pipeInner_mm,*rehynoldNumber,*prandtlColebrookLamda;
    NSMutableArray *pressurLosPerMeterArray,*pressureDropArray;
    NSMutableArray *pickerViewArray;
    int count;
    int selectedSegmet;
    NSIndexPath *calculatorIndexPath;
    NSString *pickerElement;
    
}



@property(strong,nonatomic)NSMutableDictionary *calculatorValueDict;

@property(strong,nonatomic)NSArray *pipeAndTemperatureArray;
@property(strong,nonatomic)NSArray *pressureLossArray;
@property(strong,nonatomic)NSArray *valumeOrMassFlowArray;



@end

@implementation CalculatorViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
//    NSString *password = [[NSUserDefaults standardUserDefaults]
//                          stringForKey:@"password"];
//    
//    if ([password isEqualToString:@""]) {
//        AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
//        [appdelegte.objTab setSelectedIndex:0];
//    }
//
//    else
//    {
    [super viewDidLoad];
    count = 0;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 5, 1)];
    UIBarButtonItem *spaceButton = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.rightBarButtonItems=[[NSArray alloc]initWithObjects:[self addRightBarButton1],spaceButton,[self addRightBarButton], nil];
    
//    pipeInner_m = [[NSMutableArray alloc]init];
//    A_dm  = [[NSMutableArray alloc]init];
//    volumeFRateMSec  = [[NSMutableArray alloc]init];
//    pipeInner_mm = [[NSMutableArray alloc]init];
//    rehynoldNumber =[[NSMutableArray alloc]init];
//    prandtlColebrookLamda =[[NSMutableArray alloc]init];
//    pressureDropArray =[[NSMutableArray alloc]init];
//    pressurLosPerMeterArray=[[NSMutableArray alloc]init];
//    typeofPipeArray =[[NSMutableArray alloc]init];

    
    pressureDropArray =[[NSMutableArray alloc]init];
    pressurLosPerMeterArray=[[NSMutableArray alloc]init];
    typeofPipeArray =[[NSMutableArray alloc]init];
    volumeFRateMSec  = [[NSMutableArray alloc]init];
    

    
	// Do any additional setup after loading the view.
   // self.navigationItem.title = @"Calculator";
    _tableView.sectionHeaderHeight = 0.09;
    _tableView.sectionFooterHeight = 0.09;
    
    //setup array
    
    
       //setup dictionary
    
    self.calculatorValueDict=[[NSMutableDictionary alloc] init];
    [self.calculateButton addTarget:self action:@selector(calculateButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.clearAllButton addTarget:self action:@selector(clearButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.calculateButton.layer setCornerRadius:5.0];
    [self.clearAllButton.layer setCornerRadius:5.0];
    self.tableView.backgroundColor = [UIColor whiteColor];
    //}
    
}
-(UIBarButtonItem *)addRightBarButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 20, 15)];
//        [button setTitle:@"Share" forState:UIControlStateNormal];
//    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
   
    [button addTarget:self action:@selector(shareButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    return rightItem;
    
    
}

-(UIBarButtonItem *)addRightBarButton1
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 20, 15)];
    //        [button setTitle:@"Share" forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"feedback"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(messageButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    return rightItem;
    
    
   
}
-(void)viewWillAppear:(BOOL)animated
{
     self.navigationItem.hidesBackButton = YES;
    [self.tabBarController.tabBar setHidden:NO];
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    if (![password isEqualToString:@"Bru99"]) {
        [self performActionOnPassword];
    }

   else{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"])
    {
        [Language setLanguage:@"en"];
    }
    
    else
    {
        [Language setLanguage:@"de"];
    }
    
    self.navigationItem.title= [Language get:@"Calculator" alter:@"title not found"];
    
    self.pipeAndTemperatureArray=[[NSArray alloc] initWithObjects:[Language get:@"Pipe Type" alter:@"title not found"],[Language get:@"Length Of Pipe L [m]" alter:@"title not found"],[Language get:@"Power Output Q [kW]" alter:@"title not found"],[Language get:@"Water temperature TMed [°C]" alter:@"title not found"],[Language get:@"Difference water temperature ΔT [°C]" alter:@"title not found"], nil];
    self.pressureLossArray=[[NSArray alloc] initWithObjects:[Language get:@"max. pressure loss Δpmax [Pa]" alter:@"title not found"],[Language get:@"Glykol concentation c [%]" alter:@"title not found"], nil];
    self.valumeOrMassFlowArray=[[NSArray alloc] initWithObjects:[Language get:@"Volume or mass flow" alter:@"title not found"],[Language get:@"ṁ [l/h]" alter:@"title not found"],[Language get:@"ṁ [kg/h]" alter:@"title not found"], nil];
       [self.calculateButton setTitle:[Language get:@"Calculate" alter:@"title not found"] forState:UIControlStateNormal];
       [self.clearAllButton setTitle:[Language get:@"Clear All" alter:@"title not found"] forState:UIControlStateNormal];
    
    [self.tableView reloadData];
    }
    

}
-(void)performActionOnPassword
{
    CalcLoginViewController *objCalculatorViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CalcLoginViewController"];
    [self.navigationController pushViewController:objCalculatorViewController animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section==0)
    {
        return self.pipeAndTemperatureArray.count;
        
    }if(section==1)
    {
        return self.pressureLossArray.count;
        
    }if(section==2)
    {
        return self.valumeOrMassFlowArray.count;
        
    }
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *) indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



////// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//    // self.fetchedRecordsArray=[self getEntityCount];
//
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//        deleteIndexPath=indexPath;
//        [self showAlert];
//    }
//
//}
//

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section!=3)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
        UITextField *textField;
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
       
        textField = [[UITextField alloc] initWithFrame:CGRectMake(230, 7, 80, 30)];
        textField.tag=[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue];
        textField.borderStyle=UITextBorderStyleNone;
        textField.textAlignment = NSTextAlignmentRight;
        
        
        textField.font = [UIFont systemFontOfSize:15];
        //textField.placeholder = @"";
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.returnKeyType = UIReturnKeyDone;
        //textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.userInteractionEnabled=YES;
        textField.delegate = self;
        textField.textColor = [UIColor colorWithRed:69/255.0 green:146/255.0 blue:239/255.0 alpha:1];
        
        for(UIView *subview in cell.contentView.subviews)
        {
            if([subview isKindOfClass: [UITextField class]])
            {
                [subview removeFromSuperview];
            }
        }
        if (textField.tag==13) {

        [textField setPlaceholder:[self placeHolderMethod:pipeType]];
        }
        
       
        
        
        [cell.contentView addSubview:textField];
        UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        
        
        UIButton* doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [doneButton setFrame:CGRectMake(260, 0, 60, 30)];
        [doneButton setTitle:[Language get:@"Done" alter:@"title not found"] forState:UIControlStateNormal];
        [doneButton setTitleColor:[UIColor colorWithRed:61/255.0 green:126/255.0 blue:254/255.0 alpha:1] forState:UIControlStateNormal];
        [doneButton addTarget:self action:@selector(inputAccessoryViewDidFinish1) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *doneButton1 =
        [[UIBarButtonItem alloc] initWithCustomView:doneButton];

        
//        UIBarButtonItem *doneButton1 =
//        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
//                                                      target:self action:@selector(inputAccessoryViewDidFinish1)];
        
        UIToolbar *myToolbar1 = [[UIToolbar alloc] initWithFrame:
                                 CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
        
        //using default text field delegate method here, here you could call
        //myTextField.resignFirstResponder to dismiss the views
        
        
        UIButton* customButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [customButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
        [customButton1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [customButton1 addTarget:self action:@selector(cancelPickerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customButton1 sizeToFit];
        UIBarButtonItem* customBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:customButton1];
        
        [myToolbar1 setItems:[NSArray arrayWithObjects:flexibleSpace,doneButton1, nil] animated:NO];
        
        
        textField.inputAccessoryView = myToolbar1;
        textField.inputAccessoryView.backgroundColor=[UIColor darkGrayColor];
     
      
         // UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(230, 7, 80, 30)];
        textField.borderStyle=UITextBorderStyleNone;
        //textField.text=[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section,(long)indexPath.row] ;
        
        //textField.tag=[[NSString stringWithFormat:@"%ld %ld",(long)indexPath.section,(long)indexPath.row] intValue];
        //textField.tag=[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue];
    
        if ([self.calculatorValueDict count]==0) {
            UITextField *textfield1 =(UITextField *)[self.view viewWithTag:[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue]];
            textfield1.text=@"";
            
           
            //textfield1.tag
        }
        else
        {
            UITextField *textfield1 =(UITextField *)[self.view viewWithTag:[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue]];
        textfield1.text=[self.calculatorValueDict valueForKey:[NSString stringWithFormat:@"%ld",(long)textField.tag]];
            
            // for displaying default value max pressure loss
            
            NSLog(@"textfield1.tag=%ld",(long)textField.tag);
            if (textfield1.tag == 20 && ([pipeType isEqualToString:@"Coolflex"]||[pipeType isEqualToString:@"Eigerflex"]||[pipeType isEqualToString:@"Calpex Sanitär"])) {
                if ([textField.text isEqualToString:@""]) {
                    textField.text = @"120";
                    [self.calculatorValueDict setValue:textField.text forKey:[NSString stringWithFormat:@"%ld",(long)textField.tag]];
                }
                maxPressureLoss = [[self.calculatorValueDict valueForKey:@"20"] intValue];
            }
            
           ////////////////////////////////////////////
            
        }
        NSLog(@"Textfield TAAAAAAG %ld",(long)textField.tag);
//        textField.borderStyle = UITextBorderStyleRoundedRect;
        
        
        if(indexPath.section==0)
        {
            if(indexPath.row==0)
            {
                CGRect textfieldframe=[textField frame];
                textfieldframe.origin.x=170;
                textfieldframe.size.width=140;
                [textField setFrame:textfieldframe];
                
               UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
                myPickerView.delegate = self;
                myPickerView.showsSelectionIndicator = YES;
                [myPickerView setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
                
                textField.inputView = myPickerView;
                
                
                

            }
            cell.textLabel.text= [self.pipeAndTemperatureArray objectAtIndex:indexPath.row];
            //cell.detailTextLabel.text= @"detail Title";
            
        }
        if(indexPath.section==1)
        {
            
            if(indexPath.row==1)
            {
                UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
                myPickerView.delegate = self;
                myPickerView.showsSelectionIndicator = YES;
                [myPickerView setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
                
                textField.inputView = myPickerView;
                
//                
//                UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//                
//                
//                UIBarButtonItem *doneButton1 =
//                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
//                                                              target:self action:@selector(inputAccessoryViewDidFinish1)];
//                
//                UIToolbar *myToolbar1 = [[UIToolbar alloc] initWithFrame:
//                                         CGRectMake(0,0, 320, 44)]; //should code with variables to support view resizing
//                
//                //using default text field delegate method here, here you could call
//                //myTextField.resignFirstResponder to dismiss the views
//                
//                
//                UIButton* customButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
//                [customButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
//                [customButton1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//                [customButton1 addTarget:self action:@selector(cancelPickerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//                [customButton1 sizeToFit];
//                UIBarButtonItem* customBarButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:customButton1];
//                
//                [myToolbar1 setItems:[NSArray arrayWithObjects:doneButton1,flexibleSpace,customBarButtonItem1, nil] animated:NO];
//                
//                
//                textField.inputAccessoryView = myToolbar1;
//                textField.inputAccessoryView.backgroundColor=[UIColor darkGrayColor];
//                
                
            }

            
            
            
            cell.textLabel.text= [self.pressureLossArray objectAtIndex:indexPath.row];
            //cell.detailTextLabel.text= @"detail Title";
            
        }
        if(indexPath.section==2)
        {
            calculatorIndexPath = indexPath;
            
            if(indexPath.row==0)
            {
                [textField removeFromSuperview];
            }
            NSArray *itemArray = [NSArray arrayWithObjects: [Language get:@"Volume" alter:@"Title not found" ], [Language get:@"Massflow" alter:@"Title not found" ], nil];
            UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
            segmentedControl.frame = CGRectMake(26, 7, 270,30 );
            [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents: UIControlEventValueChanged];
            segmentedControl.selectedSegmentIndex = selectedSegmet;
            //selectedSegmet =  segmentedControl.selectedSegmentIndex;
            [self segmentAction:segmentedControl];
           UITextField *massTextfiled =(UITextField *)[self.view viewWithTag:32];
            if (selectedSegmet == 0) {
                massTextfiled.userInteractionEnabled=NO;
            }
           
            switch (indexPath.row) {
                case 0:
                    
                    for(UIView *subview in cell.contentView.subviews)
                    {
                        if([subview isKindOfClass: [UISegmentedControl class]])
                        {
                            [subview removeFromSuperview];
                        }
                    }
                    //cell.textLabel.text= [self.valumeOrMassFlowArray objectAtIndex:indexPath.row];
                    [cell.contentView addSubview:segmentedControl];
                    
                    break;
                    
                    case 1:
                    cell.textLabel.text= [self.valumeOrMassFlowArray objectAtIndex:indexPath.row];
                    break;
                    case 2:
                    cell.textLabel.text= [self.valumeOrMassFlowArray objectAtIndex:indexPath.row];
                    break;
                default:
                    break;
            }
            
            
            
            
            
            //cell.detailTextLabel.text= @"detail Title";
            
        }
        
        cell.textLabel.font=[UIFont fontWithName:@"Arial" size:15];
        //cell.detailTextLabel.textColor=[UIColor blueColor];
        
        return cell;
        
    }
    else
    {
        
        CalculatorCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:@"myCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"CalculatorCustomCell" bundle:nil ] forCellReuseIdentifier:@"myCell"];
            cell=[tableView dequeueReusableCellWithIdentifier:@"myCell"];
        }
        cell.resultLabel.text=[Language get:@"Result" alter:@"title not found"];
        cell.pipeDimentionLabel.text=[Language get:@"Pipe Dimension Type" alter:@"title not found"];
        cell.pressureDropLabel.text=[Language get:@"Pressure drop Δptot [Pa]" alter:@"title not found"];
        cell.pressureLossLabel.text =[Language get:@"Pressure loss per meter Δp/m [Pa]" alter:@"title not found"];
        cell.velocityLabel.text =[Language get:@"Velocitiy of flow v [m/s]" alter:@"title not found"];
        cell.pipeTypeLabel.text = [Language get:@"Pipe Type" alter:@"title not found"];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        
        NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"languageName"];
        
        if ([savedValue isEqualToString:@"English"])
        {
//            [cell.calculateButton removeFromSuperview];
//            [cell.clearAllButton removeFromSuperview];
//            CGRect frame = [cell.calculateButton frame];
//            frame.origin.x=41;
//            frame.origin.y=17;
//            frame.size.width = 120;
//            frame.size.height=30;
//            [cell.calculateButton setFrame:frame];
//            [cell.contentView addSubview:cell.calculateButton];
//            NSLog(@"%f",[cell.calculateButton frame].origin.x);
//            NSLog(@"%f",[cell.calculateButton frame].size.width);
//            CGRect  frame1 =[cell.clearAllButton frame];
//            frame1.origin.x=172;
//            frame1.origin.y=17;
//            frame1.size.width = 120;
//            frame1.size.height=30;
//            [cell.clearAllButton setFrame:frame1];
//            [cell.contentView addSubview:cell.clearAllButton];
//            NSLog(@"%f",[cell.clearAllButton frame].origin.x);
//            NSLog(@"%f",[cell.clearAllButton frame].size.width);
        }
        
        else
        {
//            [cell.calculateButton removeFromSuperview];
//            [cell.clearAllButton removeFromSuperview];
//
//            CGRect frame = [cell.calculateButton frame];
//            frame.origin.x=15;
//            frame.origin.y=17;
//            frame.size.width = 140;
//            frame.size.height=30;
//            [cell.calculateButton setFrame:frame];
////            [cell.contentView addSubview:cell.calculateButton];
//            NSLog(@"%f",[cell.calculateButton frame].origin.x);
//          CGRect  frame1 =[cell.clearAllButton frame];
//            frame1.origin.x=142;
//            frame1.origin.y=17;
//            frame1.size.width = 160;
//            frame1.size.height=30;
//            [cell.clearAllButton setFrame:frame1];
//            [cell.contentView addSubview:cell.clearAllButton];
//            NSLog(@"%f",[cell.clearAllButton frame].origin.x);
//            NSLog(@"%f",[cell.clearAllButton frame].size.width);


        }
        
        
        
        
        [cell.calculateButton setTitle:[Language get:@"Calculate" alter:@"title not found"] forState:UIControlStateNormal];
        [cell.clearAllButton setTitle:[Language get:@"Clear All" alter:@"title not found"] forState:UIControlStateNormal];

//        [cell.calculateButton addTarget:self action:@selector(calculateButtonAction) forControlEvents:UIControlEventTouchUpInside];
//        [cell.clearAllButton addTarget:self action:@selector(clearButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        if ([pressureDropArray count]!=0) {
            cell.pressureDropResultLabel.text=[NSString stringWithFormat:@"%.3f",[[pressureDropArray objectAtIndex:index] floatValue]];
            cell.pressureLossResultLabel.text=[NSString stringWithFormat:@"%.3f",[[pressurLosPerMeterArray objectAtIndex:index]floatValue]];
            cell.pipeDimentionResultLabel.text=[NSString stringWithFormat:@"%@",[typeofPipeArray objectAtIndex:index] ];
            cell.velocityResultLabel.text=[NSString stringWithFormat:@"%.3f",[[volumeFRateMSec objectAtIndex:index] floatValue]];
            cell.pipeTypeResultLabel.text =[NSString stringWithFormat:@"%@",[self.calculatorValueDict valueForKey:@"10"] ];

            
            NSLog(@"pressureDrop=%@",[pressureDropArray objectAtIndex:index]);
            

        }
        else
        {
            cell.pressureDropResultLabel.text=@"";
            cell.pressureLossResultLabel.text=@"";
            cell.pipeDimentionResultLabel.text=@"";
            cell.velocityResultLabel.text=@"";
            cell.pipeTypeResultLabel.text=@"";

        }
        
       

         cell.pressureLossLabel.font=[UIFont fontWithName:@"Arial" size:15];
         cell.pipeDimentionLabel.font=[UIFont fontWithName:@"Arial" size:15];
         cell.pressureDropLabel.font=[UIFont fontWithName:@"Arial" size:15];
         cell.velocityLabel.font=[UIFont fontWithName:@"Arial" size:15];
        cell.pipeTypeLabel.font=[UIFont fontWithName:@"Arial" size:15];

        
        return cell;
    }
    
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==3)
    {
        return 350;
    }
    return 44;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.01;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}



#pragma mark Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    BarViewController *objBarViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BarViewController"];
    //    objBarViewController.temperatureId=[[tempArray objectAtIndex:indexPath.row] valueForKey:@"quoteid"];
    //
    //
    //    [self.navigationController pushViewController:objBarViewController animated:YES];
//    if (indexPath.section==0 && indexPath.row==0) {
//       UITextField *text= (UITextField*)[self.view viewWithTag:[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section,(long)indexPath.row] intValue]] ;
//        [text becomeFirstResponder];
//    }
  
    [[self.view viewWithTag:[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue]] becomeFirstResponder];
    
    NSLog(@"%i",[[NSString stringWithFormat:@"%ld%ld",(long)indexPath.section+1,(long)indexPath.row] intValue]);
}





- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // code for scrolling table view automatically up one bu one when click on next textfield
    CGPoint textPosition = [textField convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexpath = [self.tableView indexPathForRowAtPoint:textPosition];
    [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    ////////////////////////////////
    if (textField.tag==10)
    {
        
        AppDelegate *app=[UIApplication sharedApplication].delegate;
        
        
        NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
        
        
        [fetchReq setEntity:[NSEntityDescription entityForName:@"PipeTypes" inManagedObjectContext:app.managedObjectContext]];
        
        NSArray *tempArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:fetchReq error:nil]];
        pickerViewArray = [[NSMutableArray alloc]init];
        for (PipeTypes *obj in tempArray)
        {
            [pickerViewArray addObject:obj.pipeName];
            
        }
        
        NSLog(@"pickerViewArray=%@",pickerViewArray);
    }
    
    else if (textField.tag==21)
    {
        
    pickerViewArray = [[NSMutableArray alloc]initWithObjects:@"keine",@"20",@"25",@"30",@"35",@"40" ,nil] ;
            
    }
    else if (textField.tag==31)
    {
        
        
        
        if (powerOutput!=0) {

            textField.userInteractionEnabled=NO;
        }
        else if (selectedSegmet==1)
        {
            textField.userInteractionEnabled=NO;
        }
        else
        {
            textField.userInteractionEnabled=YES;
        }

        
        
        
    }
    else if (textField.tag==32)
    {
        if (powerOutput!=0 ) {
            

            textField.userInteractionEnabled=NO;

        }
        else if (selectedSegmet==0)
        {
            textField.userInteractionEnabled=NO;
        }
        else
        {
            textField.userInteractionEnabled=YES;
        }
        
    }
    
    else if (textField.tag==14)
    {
        UITextField *waterTempraturefield =(UITextField *)[self.view viewWithTag:13];
        BOOL response = [self checkWaterTemprature:waterTempraturefield];
        if (response == YES) {
            WaterTemperature=[waterTempraturefield.text intValue];
        }
        else{
            [self inputAccessoryViewDidFinish1];
            return NO;
        }

    }
     if (textField.tag==12)
    {
        if (massFlowKgHr!=0 || massFlowLitHr!=0) {

            textField.userInteractionEnabled=NO;

        }
        else
        {
             textField.userInteractionEnabled=YES;
        }
    }

    
   
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGPoint textPosition = [textField convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexpath = [self.tableView indexPathForRowAtPoint:textPosition];

    UITextField *field1 ;
    bool response;
    switch (textField.tag) {
            
        case 10:
            
            textField.text= [pickerViewArray objectAtIndex:count];
            pipeType=textField.text;
            //[self placeHolderMethod:textField.text];
            pickerViewArray=nil;
            count =0;
            
            break;
            case 11:
            lengthofPipe=[textField.text intValue];
            break;
            
        case 12:
            powerOutput=[textField.text intValue];
            
           field1 =(UITextField *)[self.view viewWithTag:31];
            if (powerOutput!=0) {
                field1.userInteractionEnabled=NO;
                
            field1=(UITextField *)[self.view viewWithTag:32];
                field1.userInteractionEnabled=NO;
                
            }
            else
            {
               
                if (selectedSegmet==0)
                {
                    field1=(UITextField *)[self.view viewWithTag:31];
                    field1.userInteractionEnabled=YES;
                    field1=(UITextField *)[self.view viewWithTag:32];
                    field1.userInteractionEnabled=NO;

                }
                
                else if (selectedSegmet == 1)
                {
                    field1=(UITextField *)[self.view viewWithTag:32];
                    field1.userInteractionEnabled=YES;
                    field1=(UITextField *)[self.view viewWithTag:31];
                    field1.userInteractionEnabled=NO;

                }
                
                
                
            }

    
            
            break;
        case 13:
            response = [self checkWaterTemprature:textField];
            if (response == YES) {
                WaterTemperature=[textField.text intValue];
            }
            else{
                NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                        stringForKey:@"languageName"];
                
                if ([savedValue isEqualToString:@"English"])
                {
                    [Language setLanguage:@"en"];
                    NSLog(@"%@",[Language get:@"Please_enter_value_in_specified_range" alter:@"Title not found"]);
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[Language get:@"Alert"alter:@"No Title"] message:@"Please enter value in specified range" delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
                    alert.tag = 13;
                    [alert show];
                }
                
                else
                {
                    [Language setLanguage:@"de"];
                    NSLog(@"%@",[Language get:@"Please_enter_value_in_specified_range" alter:@"Title not found"]);
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[Language get:@"Alert"alter:@"No Title"] message:[Language get:@"Please_enter_value_in_specified_range" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
                    alert.tag = 13;
                    [alert show];
                }
                
//                textField.text = @"";
            }
           // WaterTemperature=[textField.text intValue];
            break;
        case 14:
            differenceWaterTemperature=[textField.text intValue];
            break;
        case 20:
            maxPressureLoss=[textField.text intValue];
            break;
        case 21:
            
            textField.text= [pickerViewArray objectAtIndex:count];
            if ([textField.text isEqualToString:@"keine" ]) {
                glykolConcent=1;
            }
            else
            {
            glykolConcent=[textField.text intValue];
                
            }
            
            pickerViewArray=nil;
             // code for scrolling table view automatically up one bu one when click on next textfield
            [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            /////////////////////////////
            break;
        case 31:
           field1 =(UITextField *)[self.view viewWithTag:12];
            
            
            if ([textField.text length]!=0)
            {
                
                field1.userInteractionEnabled=NO;
            }
            else
            {
                field1.userInteractionEnabled=YES;
            }
            
            massFlowLitHr=[textField.text intValue];
            break;
        case 32:
            field1 =(UITextField *)[self.view viewWithTag:12];
            if ([textField.text length]!=0) {
                [textField resignFirstResponder];
                
                
                
                field1.userInteractionEnabled=NO;
            }
            else
            {
                field1.userInteractionEnabled=YES;
            }
            massFlowKgHr=[textField.text intValue];
            break;
            
        default:
            break;
    }
    
    NSLog(@"Textfield tag= %ld",(long)textField.tag);
    
    // Add value to dictionary
    
    [self.calculatorValueDict setValue:textField.text forKey:[NSString stringWithFormat:@"%ld",(long)textField.tag]];
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;

}

#pragma mark UIPickerView Datasource and Delefgate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    count=(int)row;
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [pickerViewArray count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    NSString *title;
//    title = [subjectsNameArray objectAtIndex:row];
//
//    return title;
//}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        tView.font = [UIFont systemFontOfSize:15.0];
        tView.text = [pickerViewArray objectAtIndex:row] ;
        tView.textAlignment = NSTextAlignmentCenter;
    }
    // Fill the label text here
    return tView;
}

#pragma mark InputAccessory Methods
-(void)inputAccessoryViewDidFinish1

{
    pickerElement = [pickerViewArray objectAtIndex:count];
    // for displaying default value max pressure loss
    
    
    if ([pickerElement isEqualToString:@"Coolflex"]||[pickerElement isEqualToString:@"Eigerflex"]||[pickerElement isEqualToString:@"Calpex Sanitär"]||[pickerElement isEqualToString:@"Calpex Heizung"])
    {
        [self.calculatorValueDict setValue:@"" forKey:@"20"];
        [self.calculatorValueDict setValue:@"" forKey:@"13"];
        if ([ self.calculatorValueDict valueForKey:@"21"]==nil) {
            [self.calculatorValueDict setValue:@"0%" forKey:@"21"];
            glykolConcent=1;
        }
        
    }
    
    
    // for displaying default value max pressure loss
    //[self.calculatorValueDict setValue:@"" forKey:@"20"];
    /////////////////////////////////////
    [self.view endEditing:YES];
    [self.tableView reloadData];
    
    
    //
}
-(void)cancelPickerButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    
}
#pragma mark calculator Methods
-(BOOL)checkWaterTemprature:(UITextField *)tempratureTextField
{
    int temprature = [tempratureTextField.text intValue];
    if ([pipeType isEqualToString:@"Calpex Heizung"]) {
        // temprature.placeholder =@"(30 - 95 °C)";
        
        if (temprature>=30 && temprature<=95) {
            return YES;
        }
        else
        {
            return NO;
        }
            }
    else if ([pipeType isEqualToString:@"Eigerflex"])
    {
        // temprature.placeholder =@"(30 - 80 °C)";
        if (temprature>=30 && temprature<=80) {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if ([pipeType isEqualToString:@"Calpex Sanitär"]) {
        //temprature.placeholder = @"(-20 - 40 °C)";
        if (temprature>= -20 && temprature<=40) {
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    else if ([pipeType isEqualToString:@"Coolflex"]) {
        // temprature.placeholder = @"(-20 - 40 °C)";
        if (temprature>= -20 && temprature<=40) {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return NO;
}

-(NSString *)placeHolderMethod:(NSString*)text
{
   // UITextField *temprature = (UITextField *)[self.view viewWithTag:10];
    
    if ([text isEqualToString:@"Calpex Heizung"]) {
      // temprature.placeholder =@"(30 - 95 °C)";
        return @"(30 - 95 °C)";
    }
    else if ([text isEqualToString:@"Eigerflex"]) {
       // temprature.placeholder =@"(30 - 80 °C)";
        return @"(30 - 80 °C)";
    }
   else if ([text isEqualToString:@"Calpex Sanitär"]) {
        //temprature.placeholder = @"(-20 - 40 °C)";
       return  @"(-20 - 40°C)";
    }
   else if ([text isEqualToString:@"Coolflex"]) {
       // temprature.placeholder = @"(-20 - 40 °C)";
       return  @"(-20 - 40°C)";
    }
    return @"";
    
}
-(void)segmentAction:(UISegmentedControl *)segment
{
    UITextField *volumeTextFiled = (UITextField *)[self.view viewWithTag:31];
     UITextField *massTextFiled = (UITextField *)[self.view viewWithTag:32];
    
    selectedSegmet=segment.selectedSegmentIndex;
    
    if (selectedSegmet==0 && powerOutput == 0) {
        volumeTextFiled.userInteractionEnabled=YES;
        massTextFiled.userInteractionEnabled=NO;
        massTextFiled.text = @"";
    }
    
    else if (selectedSegmet==1 && powerOutput == 0)
    {
        volumeTextFiled.text = @"";
        volumeTextFiled.userInteractionEnabled=NO;
        massTextFiled.userInteractionEnabled=YES;
    }
    else{
        massTextFiled.userInteractionEnabled=NO;
        volumeTextFiled.userInteractionEnabled=NO;

    }
}
-(void)calculateButtonAction
{
    [self.view endEditing:YES];
    [self.tableView reloadData];
 NSArray *destinationKeys = self.calculatorValueDict.allKeys;
    
    for (NSString *key in destinationKeys)
    {
        id value = [self.calculatorValueDict valueForKey:key];
        // Avoid NULL values
        if (value && ![value isEqual:[NSNull null]])
        {
            // for setting value of 0% glycol to 1
            if([value isEqualToString:@""] || [value isEqualToString:@"0"])
            {
                [self.calculatorValueDict removeObjectForKey:key];
            }
            
            if (glykolConcent == 1 && WaterTemperature<30) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get:@"Alert" alter:@"Title not found"] message:[Language get:@"Please enter temprature greater than 29" alter:@"Please enter temprature greater than 29"] delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
                [alert show];
                return;

            }
        }
    }

    NSArray *destinationKeys1 = self.calculatorValueDict.allKeys;

 if(destinationKeys1.count<7)
 {
     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get:@"Alert" alter:@"Title not found"] message:[Language get:@"Please enter required values" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
     [alert show];
     return;
 }
    
    
    //Reset  all arrays
    [pressureDropArray removeAllObjects];
    [pressurLosPerMeterArray removeAllObjects];
    [typeofPipeArray removeAllObjects];
    [volumeFRateMSec removeAllObjects];
    
    
    pipeInner_m = [[NSMutableArray alloc]init];
    A_dm  = [[NSMutableArray alloc]init];
    
    pipeInner_mm = [[NSMutableArray alloc]init];
    rehynoldNumber =[[NSMutableArray alloc]init];
    prandtlColebrookLamda =[[NSMutableArray alloc]init];
    
    if ([[self.calculatorValueDict valueForKey:@"21"] isEqualToString:@"0%"]) {
        glykolConcent=1;
    }
    
    
    //1)calculting waterDensity ρ(TMed] [kg/m3] from Glykol or warter database table
    
    AppDelegate *app=[UIApplication sharedApplication].delegate;
    
    NSFetchRequest *fetchReq = [[NSFetchRequest alloc]init];
    
    if (glykolConcent==1) {
        [fetchReq setEntity:[NSEntityDescription entityForName:@"Warter" inManagedObjectContext:app.managedObjectContext]];
        
        NSArray *warterArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:fetchReq error:nil]];
        
        Warter *objwarter;
        if ([warterArray count]!=0) {
            objwarter = [warterArray objectAtIndex:0];
        }
        
        NSArray *warterDetailArray=[objwarter.warterVariableDetail allObjects];
        
        for (WarterVariableDetail *obj in warterDetailArray) {
            if ([obj.temrature integerValue]==WaterTemperature) {
                waterDensity=[obj.density floatValue];
                break;
            }
        }
        glykolString=@" ";
        
    }
    
    else
    {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.concentration=%d",glykolConcent ];
        [fetchReq setPredicate:pred];
        
        [fetchReq setEntity:[NSEntityDescription entityForName:@"GlykolConcentration" inManagedObjectContext:app.managedObjectContext]];
        NSError *error;
        NSArray *comcentrationArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:fetchReq error:&error]];
        GlykolConcentration *concentration;
        if ([comcentrationArray count ]!=0)
        {
            concentration = [comcentrationArray objectAtIndex:0];
        }
        
        NSArray *concentrationDetailsArray = [concentration.concentraionDetail allObjects];
        for (ConcentraionDetail *cDetails in concentrationDetailsArray)
        {
            if ([cDetails.temrature intValue]==WaterTemperature)
            {
                waterDensity =[cDetails.concentrationValue floatValue];
                break;
            }
            
        }
    }
    
    //2)Massflowm(kg/h) =(Q[kw] * 860) / ΔT [°C] and  m(kg/s) =  m(kg/h)/3600
    
    if (powerOutput==0)
    {
        volumeFRateLHr=massFlowLitHr;
        
        float tempVls1 =volumeFRateLHr/3600;
        float tempVls2=(massFlowKgHr/waterDensity)*1000/3600;
        if (tempVls1 >0)
        {
            volumeFRateLSec=tempVls1;
        }
        else
        {
            volumeFRateLSec=tempVls2;
        }
        
    }
    else
    {
        //Massflowm(kg/h) =(Q[kw] * 860) / ΔT [°C] and  m(kg/s) =  m(kg/h)/3600
        massFlowKgHr=(powerOutput * 860)/differenceWaterTemperature;
        
        massFlowKgSec =massFlowKgHr/3600;
        
        //3)Volume Flow Rate v[l/h] = ( m(kg/h)/ ρ(TMed] [kg/m3] )*1000 or Volume Flow Rate v[l/s] = ( m(kg/s)/ ρ(TMed] [kg/m3] )*1000
    
        volumeFRateLHr = (massFlowKgHr/waterDensity) * 1000;
        
        volumeFRateLSec = (massFlowKgSec/waterDensity) * 1000;
        
    }
    
   // 3)Dynamic viscosity  η(TMed) [μkg/ms]: for this lookup in database  warter and pick up dynamic viscosity  for the temperature Tmed [°C].

    NSFetchRequest *freq =[[NSFetchRequest alloc]init];
    
    [freq setEntity:[NSEntityDescription entityForName:@"Warter" inManagedObjectContext:app.managedObjectContext]];
    
    NSArray *warterArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:freq error:nil]];
    
    Warter *objwarter = [warterArray objectAtIndex:0];
    
    NSArray *warterDetailArray=[objwarter.warterVariableDetail allObjects];
    
    for (WarterVariableDetail *obj in warterDetailArray) {
        if ([obj.temrature intValue]==WaterTemperature)
        {
            dynamicViscosityMuekg=[obj.viscocity floatValue];
            break;
        }
    }
    dynamicViscositykg = dynamicViscosityMuekg * 0.000001 ;
    
    
    // 4)Kinematic viscosity ν(TMed) [μm2/s] :
    
    // a) ν(TMed) [μm2/s]=η(TMed) [μkg/ms] / ρ(TMed] [kg/m3] )
    // b) ν(TMed) [m2/s]=(1.78*10-6) / [(1+0.0337 * Tmed [°C] +0.000221 *(Tmed [°C]  )2) ]
    
    kinematicViscosityMuem2 =(dynamicViscosityMuekg/waterDensity);
    
    float kinematic1 =(1+(0.0337 * WaterTemperature )+(0.000221 *  WaterTemperature*WaterTemperature)) ;
    
    kinematicViscositym2  = (0.00000178)/kinematic1;
    
    
    //6)Selection of pipe table:-Select pipe table based on pipe selection type in drop down list  go to that pipe table and pickup that pipe table . out of this table take the values of column “pipeType” and “Inner-ø” .name this “Inner-ø” as “Indoor-ø [mm]”.
    
    //    a) Indoor-ø [m] = Indoor-ø [mm] / 1000
    
    //    b) A [dm^2] = [Indoor-ø [mm] / 200]*PI()
    
    //    c) v [m/s] = [Volume Flow Rate v[l/s] / A [dm^2] ]  / 10
    
    
    // fetch pipeInner from DB and put in pipeInner_mm array
    
    NSFetchRequest *freq1 =[[NSFetchRequest alloc]init];
    
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"self.pipeName=%@",pipeType ];
    [freq1 setPredicate:pred1];
    
    [freq1 setEntity:[NSEntityDescription entityForName:@"PipeTypes" inManagedObjectContext:app.managedObjectContext]];
    
    NSArray *pipeArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:freq1 error:nil]];
    
    PipeTypes *objPipeTypes = [pipeArray objectAtIndex:0];
    
    NSArray *PipeVariableDetailArray=[objPipeTypes.pipeVariableDetail allObjects];
    
    
    for (PipeVariableDetail *objPipeVariableDetail in PipeVariableDetailArray)
    {
        [pipeInner_mm addObject:objPipeVariableDetail.pipeInner];
        [typeofPipeArray addObject:objPipeVariableDetail.pipetype];
    }
    
    NSFetchRequest *freq2 =[[NSFetchRequest alloc]init];
    
    NSPredicate  *pred2 = [NSPredicate predicateWithFormat:@"self.pipeName=%@",pipeType ];
    [freq2 setPredicate:pred2];
    [freq2 setEntity:[NSEntityDescription entityForName:@"PipeRoughness" inManagedObjectContext:app.managedObjectContext]];
    
    NSArray *pipeRoughnessArray = [[NSArray alloc]initWithArray:[app.managedObjectContext executeFetchRequest:freq2 error:nil]];
    
    PipeRoughness *objPipeRoughness = [pipeRoughnessArray objectAtIndex:0];
    pipeRoughness= [objPipeRoughness.surfaceRoughness floatValue];
    
    
    for (int l=0;l<[pipeInner_mm count];l++)
    {
        //  PipeVariableDetail *variableDetails = [pipeInner_mm objectAtIndex:l];
        float innerResult = [[pipeInner_mm objectAtIndex:l] floatValue]/1000;
        
        float Amd=([[pipeInner_mm objectAtIndex:l] floatValue]/200 );
        Amd = powf(Amd, 2) * M_PI;
        
        float vms= (volumeFRateLSec/Amd)/10;
        [pipeInner_m addObject:[NSNumber numberWithFloat:innerResult]];
        
        [A_dm addObject:[NSNumber numberWithFloat:Amd]];
        [volumeFRateMSec addObject:[NSNumber numberWithFloat:vms]];
        
        // Re=[v [m/s] * (Indoor-ø [mm] / 1000)] / ν(TMed) [m2/s]
        
        float Re =(vms * innerResult)/kinematicViscositym2 ;
        
        float lamb=0.1;
        NSMutableArray *lambArray =[[NSMutableArray alloc]init];
        [lambArray addObject:[NSNumber numberWithFloat:lamb]];
        
        
        for (int i=0; i<9; i++)
        {
            
            //lamb2=(-2*LOG(((k/ Indoor-ø [mm]) / 3.71)+(2.51 / (Re * (lamb1)1/2 ))))-2
            lamb =[[lambArray objectAtIndex:i] floatValue];
            float temp = (pipeRoughness/[[pipeInner_mm objectAtIndex:l] floatValue])/3.71;
            
            float  temp1=2.51/(Re * powf(lamb, 0.5));
            
            temp=temp+temp1;
            
            temp1=log10f(temp);
            
            temp =-2*temp1;
            
            temp1 =powf(temp, -2);
            NSString *finalLamb = [ NSString stringWithFormat:@"%.7f",temp1];
            
            [lambArray addObject:[NSNumber numberWithFloat:[finalLamb floatValue]]];
            
        }
        [prandtlColebrookLamda addObject:[lambArray objectAtIndex:9] ] ;
        
        lambArray=nil;
        
        
        //9) Pressure loss per meter Δp/m [Pa] :(waterDensity ρ(TMed] [kg/m3])
        
        // Δp/m [Pa] = (λ * ρ(TMed] [kg/m3] * (v [m/s] )2 )  / (Indoor-ø [m] * 2 )
        
        float temporary =[[prandtlColebrookLamda objectAtIndex:l] floatValue]*waterDensity *powf(vms, 2);
        
        temporary = temporary/(innerResult * 2);
        
        [pressurLosPerMeterArray addObject:[NSNumber numberWithFloat:temporary]];
        
        temporary = temporary*lengthofPipe;
        
        [pressureDropArray addObject:[NSNumber numberWithFloat:temporary]];
        
        
        
        
    }
    
    index = -1;
    
    for (int i=0; i<[pressurLosPerMeterArray count]; i++)
    {
        if (maxPressureLoss >=[[pressurLosPerMeterArray objectAtIndex:i] floatValue] ) {
            
            if (index != -1)
            {
                if ([[pressurLosPerMeterArray objectAtIndex:i] floatValue] > [[pressurLosPerMeterArray objectAtIndex:index] floatValue])
                {
                    index = i;
                }
                
            }
            else
                index=i;
            
            
        }
    }
    
    NSLog(@"%@",[pressurLosPerMeterArray objectAtIndex:index]);
    
    NSLog(@"%@",[pressureDropArray objectAtIndex:index]);
    NSLog(@"%@",[volumeFRateMSec objectAtIndex:index]);
    NSLog(@"%@",[typeofPipeArray objectAtIndex:index]);
    
    [self.tableView reloadData];
    
    
    pipeInner_m =nil;
    A_dm  = nil;
    
    pipeInner_mm = nil;
    rehynoldNumber =nil;
    prandtlColebrookLamda =nil;
    
     // code for scrolling table view automatically up one bu one when click on next textfield
    [self.tableView scrollToRowAtIndexPath:calculatorIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    ///////////////////
    
}


-(void)clearButtonAction
{
    [self.calculatorValueDict removeAllObjects];
    NSLog(@"%lu",(unsigned long)[self.calculatorValueDict count]);
    [pressureDropArray removeAllObjects];
    [pressureDropArray removeAllObjects];
    
    [volumeFRateMSec removeAllObjects];
   [ typeofPipeArray removeAllObjects];
    [self.tableView reloadData];
   
}

#pragma mark result sharing and feedback methods
-(void)shareButtonClickedAction:(UIButton *)sender
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    NSString *emailFileName = [NSString stringWithFormat:@"index.html"];
    NSString *emailFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: emailFileName];
    NSString *body = [NSString stringWithContentsOfFile:emailFilePath encoding:NSUTF8StringEncoding error:nil];
    
    
    // make sure you have the image name and extension (for demo purposes, I'm using "myImage" and "png" for the file "myImage.png", which may or may not be localized)
//    NSString *imageFileName = @"logo";
//    NSString *imageFileExtension = @"jpg";
//    
//    // load the path of the image in the main bundle (this gets the full local path to the image you need, including if it is localized and if you have a @2x version)
//    UIImage *emailImage;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Lollipopcoupon" ofType:@"pkpass"];
    //emailImage = [UIImage imageWithContentsOfFile:imagePath];;
    
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];
    
   // [objMFMailcomposeVc  addAttachmentData:imageData mimeType:nil  fileName:filePath];
//
//    // generate the html tag for the image (don't forget to use file:// for local paths)
//    body = [body stringByReplacingOccurrencesOfString:@"//logo_PLACEHOLDER//" withString:[NSString stringWithFormat:@"<img src=\"////file://%@\" />", imagePath]];
    
    
//    cell.pressureDropResultLabel.text=[NSString stringWithFormat:@"%.2f",[[pressureDropArray objectAtIndex:index] floatValue]];
//    cell.pressureLossResultLabel.text=[NSString stringWithFormat:@"%.2f",[[pressurLosPerMeterArray objectAtIndex:index]floatValue]];
//    cell.pipeDimentionResultLabel.text=[NSString stringWithFormat:@"%@",[typeofPipeArray objectAtIndex:index] ];
//    cell.velocityResultLabel.text=[NSString stringWithFormat:@"%.2f",[[volumeFRateMSec objectAtIndex:index] floatValue]];
    
    
    
    body = [body stringByReplacingOccurrencesOfString:@"Mieter" withString:@"Calculation"];
    body = [body stringByReplacingOccurrencesOfString:@"logo_PLACEHOLDER" withString:@""];
    body = [body stringByReplacingOccurrencesOfString:@"M_Geschlecht" withString:[Language get:@"Pipe Dimension Type" alter:@"No title"]];
    
     body = [body stringByReplacingOccurrencesOfString:@"PI_Vorname" withString:[Language get:@"Pressure_LossPer_meter" alter:@"No title"]];
    
    body = [body stringByReplacingOccurrencesOfString:@"Pipe_pressDrop" withString:[Language get:@"Pressure_Drop"alter:@"No title"]];
    
    body = [body stringByReplacingOccurrencesOfString:@"velocityFlow" withString:[Language get:@"Velocity_Of_Flow" alter:@"No title"]];
    
        if ([pressureDropArray count]!=0)
    {
        body = [body stringByReplacingOccurrencesOfString:@"Rizwan" withString:[NSString stringWithFormat:@"%@",[typeofPipeArray objectAtIndex:index] ]];

         body = [body stringByReplacingOccurrencesOfString:@"Ramesh" withString:[NSString stringWithFormat:@"%.2f",[[pressurLosPerMeterArray objectAtIndex:index]floatValue]]];
        body = [body stringByReplacingOccurrencesOfString:@"pressDropvalue" withString:[NSString stringWithFormat:@"%.2f",[[pressureDropArray objectAtIndex:index] floatValue]]];

        body = [body stringByReplacingOccurrencesOfString:@"velocityValue" withString:[NSString stringWithFormat:@"%.2f",[[volumeFRateMSec objectAtIndex:index] floatValue]]];

    }
        else{
            body = [body stringByReplacingOccurrencesOfString:@"Rizwan" withString:@"0"];
            
            body = [body stringByReplacingOccurrencesOfString:@"Ramesh" withString:@"0"];
            body = [body stringByReplacingOccurrencesOfString:@"pressDropvalue" withString:@"0"];
            
            body = [body stringByReplacingOccurrencesOfString:@"velocityValue" withString:@"0"];

            
        }
//    [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
//    [objMFMailcomposeVc setSubject:[Language get:@"Subject" alter:@"No title"]];
    [objMFMailcomposeVc setMessageBody:body isHTML:YES];
    
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get:@"Alert" alter:@"Title not found"] message:[Language get:@"Please Signed in" alter:@"Title not found"]  delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }

}
-(void)messageButtonClickedAction:(UIButton *)sender
{
//    FeddbackViewController *objFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeddbackViewController"];
//    
//    [self.navigationController pushViewController:objFeddbackViewController animated:YES];
    
    
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    
     [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    [objMFMailcomposeVc setSubject:@""];
    
    //[objMFMailcomposeVc setToRecipients:[NSArray arrayWithObject:@""]];
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get: @"Please Signed in" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }


}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%@",error);
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark UiAertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 13) {
        
        UITextField *textfield =(UITextField *)[self.view viewWithTag:13];
        textfield.text= @"";
        [textfield becomeFirstResponder];
        
    }
}


@end
