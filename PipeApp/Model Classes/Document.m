//
//  Document.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 12/30/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "Document.h"


@implementation Document

@dynamic docDate;
@dynamic docImageName;
@dynamic docName;
@dynamic docRecordingName;
@dynamic docTime;

@end
