//
//  DBManager.m
//
//  Created by apple on 11/11/13.
//  Copyright (c) 2013 redbytes. All rights reserved.
//

#import "DBManager.h"


static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    //NSString *docsDir;
    //NSArray *dirPaths;
    // Get the documents directory
    //dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //docsDir = dirPaths[0];
    // Build the path to the database file
    //databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: @"DhinchakApp.db"]];
    
    databasePath = [[NSBundle mainBundle] pathForResource:@"Product" ofType:@"sqlite"];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            //            const char *sql_stmt_contact =
            //            "create table if not exists ContactsDetail (contactid integer primary key autoincrement, name text, email text)";
            //            if (sqlite3_exec(database, sql_stmt_contact, NULL, NULL, &errMsg)
            //                != SQLITE_OK)
            //            {
            //                isSuccess = NO;
            //                NSLog(@"Failed to create contacts detailtable");
            //            }
            
            const char *sql_stmt_quotes =
            
            "create table if not exists Temperature (id text, maxTemperature text)";
            
            if (sqlite3_exec(database, sql_stmt_quotes, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create quotes table");
            }
            
            
            
            
            
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}


- (NSArray*)getMaxTemp
{
    databasePath = [[NSBundle mainBundle] pathForResource:@"Product.sqlite" ofType:nil];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"select * from Temperature"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                int  quoteid =sqlite3_column_int(statement, 0);
                
                NSString *quotetext =  sqlite3_column_text(statement, 1)!=NULL?[[NSString alloc]initWithUTF8String:
                                                                                    (const char *) sqlite3_column_text(statement, 1)]:nil;
                
                //    NSString *quotelogo = [NSString stringWithCString:(char *)sqlite3_column_text(statement, 1) encoding:NSASCIIStringEncoding];
                //                NSString *quotelogo = sqlite3_column_text(statement, 2)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                               (const char *) sqlite3_column_text(statement, 2)]:nil;
                //
                
                
                //                NSString *quotecategory = sqlite3_column_text(statement, 3)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                            (const char *) sqlite3_column_text(statement, 3)]:nil;
                //
                // NSData *quotelogo=[NSData dataWithBytes:sqlite3_column_blob(statement,2) length:sqlite3_column_bytes(statement,2)];
                
                NSDictionary *dataDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:quoteid ],@"quoteid",quotetext,@"quotetext", nil];
                [ resultArray addObject:dataDict];
            }
            
            sqlite3_reset(statement);
            if ([resultArray count]==0) {
                return nil;
            }
            return resultArray;
        }
    }
    return nil;
}

- (NSArray*)getMaxBarValueWithId:(NSString *)ID;
{
    databasePath = [[NSBundle mainBundle] pathForResource:@"Product.sqlite" ofType:nil];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"select * from Bar where id ='%@'",ID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                int  quoteid =sqlite3_column_int(statement, 0);
                
                NSString *quotetext =      sqlite3_column_text(statement, 1)!=NULL?[[NSString alloc]initWithUTF8String:
                                                                                    (const char *) sqlite3_column_text(statement, 1)]:nil;
                
                //    NSString *quotelogo = [NSString stringWithCString:(char *)sqlite3_column_text(statement, 1) encoding:NSASCIIStringEncoding];
                //                NSString *quotelogo = sqlite3_column_text(statement, 2)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                               (const char *) sqlite3_column_text(statement, 2)]:nil;
                //
                
                
                //                NSString *quotecategory = sqlite3_column_text(statement, 3)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                            (const char *) sqlite3_column_text(statement, 3)]:nil;
                //
                // NSData *quotelogo=[NSData dataWithBytes:sqlite3_column_blob(statement,2) length:sqlite3_column_bytes(statement,2)];
                
                NSDictionary *dataDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:quoteid ],@"quoteid",quotetext,@"quotetext", nil];
                [ resultArray addObject:dataDict];
            }
            
            sqlite3_reset(statement);
            if ([resultArray count]==0) {
                return nil;
            }
            return resultArray;
        }
    }
    return nil;
    
    
}

- (NSArray*)getMaxDimentionWithId:(NSString *)ID;
{
    databasePath = [[NSBundle mainBundle] pathForResource:@"Product.sqlite" ofType:nil];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"select * from Dimension where barId ='%@'",ID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                int  quoteid =sqlite3_column_int(statement, 0);
                
                NSString *quotetext =      sqlite3_column_text(statement, 1)!=NULL?[[NSString alloc]initWithUTF8String:
                                                                                    (const char *) sqlite3_column_text(statement, 1)]:nil;
                
                //    NSString *quotelogo = [NSString stringWithCString:(char *)sqlite3_column_text(statement, 1) encoding:NSASCIIStringEncoding];
                //                NSString *quotelogo = sqlite3_column_text(statement, 2)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                               (const char *) sqlite3_column_text(statement, 2)]:nil;
                //
                
                
                //                NSString *quotecategory = sqlite3_column_text(statement, 3)!=NULL?[[NSString alloc]initWithUTF8String:
                //                                                                            (const char *) sqlite3_column_text(statement, 3)]:nil;
                //
                // NSData *quotelogo=[NSData dataWithBytes:sqlite3_column_blob(statement,2) length:sqlite3_column_bytes(statement,2)];
                
                NSDictionary *dataDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:quoteid ],@"quoteid",quotetext,@"quotetext", nil];
                [ resultArray addObject:dataDict];
            }
            
            sqlite3_reset(statement);
            if ([resultArray count]==0) {
                return nil;
            }
            return resultArray;
        }
    }
    return nil;
    
    
}

- (NSArray*)getProductImageWithTempId:(NSString *)tempId barId:(NSString *)barId dimensionId:(NSString *)dimensionId
{
    
    databasePath = [[NSBundle mainBundle] pathForResource:@"Product.sqlite" ofType:nil];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"select * from Productimage where temperatureId ='%@' and barId ='%@' and dimensionId ='%@'",tempId,barId,dimensionId];
        
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                int  tempId =sqlite3_column_int(statement, 0);
                int  barId =sqlite3_column_int(statement, 1);
                int  dimensionId =sqlite3_column_int(statement, 2);
                int  observableId =sqlite3_column_int(statement, 3);
                
                NSString *productName =      sqlite3_column_text(statement, 4)!=NULL?[[NSString alloc]initWithUTF8String:
                                                                                      (const char *) sqlite3_column_text(statement, 4)]:nil;
                
                            productName = [productName stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
                            productName = [productName stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
              
              
                
                NSDictionary *dataDict=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:tempId ],@"tempId",[NSNumber numberWithInt:barId ],@"barId",[NSNumber numberWithInt:dimensionId ],@"dimensionId",[NSNumber numberWithInt:observableId ],@"observableId",productName,@"productName", nil];
                
                [ resultArray addObject:dataDict];
            }
            
            sqlite3_reset(statement);
            if ([resultArray count]==0) {
                return nil;
            }
            return resultArray;
        }
    }
    return nil;
    
    
}



@end
