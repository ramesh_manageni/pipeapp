//
//  ProductDetailsViewController.m
//  PipeApp
//
//  Created by Redbytes on 21/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "DACustomTableCell.h"

@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController
{
    NSMutableArray *pipeImageArray;
}

@synthesize productName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [self addRightBarButton];
    self.productDetailLabel.text=[NSString stringWithFormat:@"%@ (Product Details)",productName];
   // for changing product labels background color
    self.productDetailLabel.backgroundColor =[UIColor whiteColor]; //[UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];
    self.productNameLabel.backgroundColor =[UIColor whiteColor];//[UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];
    ////////////////////////
    self.productImageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"image.png"]];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 480){
                // iPhone retina-3.5 inch
                self.applicationLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 350, 227, 55)];
                self.applicationLabel.numberOfLines = 2;
                [self.applicationLabel setFont:[UIFont systemFontOfSize:17]];
                [self.view addSubview:self.applicationLabel];
                
            } else{
                // iPhone retina-4 inch
                NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
                
                self.applicationLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 428, 227, 55)];
                self.applicationLabel.numberOfLines = 2;
                [self.applicationLabel setFont:[UIFont systemFontOfSize:17]];

                [self.view addSubview:self.applicationLabel];
            }
        }
        else {
            // not retina display
        }
    }
    self.productNameView.layer.borderColor = [UIColor blackColor].CGColor;
    self.productNameView.layer.borderWidth = 1.0;
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [self localizationAction];
    [self.tabBarController.tabBar setHidden:NO];
    if (observType!=nil) {
        NSString *temp = [NSString stringWithFormat:@"   %@    %@    %@      %@",temprature,bar,dimension,[Language get:observType alter:@"Title not found"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        NSLog(@"%@",temp);
        self.productDetailLabel.text = temp;

    }
    else
    {
        NSString *temp = [NSString stringWithFormat:@"   %@    %@    %@",temprature,bar,dimension];
        temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        NSLog(@"%@",temp);
        self.productDetailLabel.text = temp;
        
    }

      self.productNameLabel.text = productName;
    [self initializeUI];
}
-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        
    }
    else{
        [Language setLanguage:@"en"];
        
    }
    
    
}
-(void)initializeUI
{
    productName= [productName stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([productName isEqualToString:@"CASAFLEX"]) {
        //self.productImageView.image=[UIImage imageNamed:@"CFL_UNO.png"];
        self.applicationLabel.text =[Language get:@"District heating systems, solar technology" alter:@"Tilte not found"];
        pipeImageArray = nil;
        pipeImageArray = [[NSMutableArray alloc]initWithObjects:@"CFL_UNO.png",@"CFL_DUO.png", nil];
        [self addTableView];

    }
    else if ([productName isEqualToString:@"CALPEX"])
    {
        //self.productImageView.image=[UIImage imageNamed:@"Calpex ALU_DUO.png"];
        [self addImageView:@"Calpex ALU_DUO.png"];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"CALPEX ALU"]) {
       // self.productImageView.image=[UIImage imageNamed:@"Calpex ALU_DUO.png"];
        [self addImageView:@"Calpex ALU_DUO.png"];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"CALCOPPER"]) {
        //self.productImageView.image=[UIImage imageNamed:@"CCO.png"];
        [self addImageView:@"CCO.png"];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"FLEXWELL"])
    {
    
        //self.productImageView.image=[UIImage imageNamed:@"FHK.png"];
        [self addImageView:@"FHK.png"];
        self.applicationLabel.text =[Language get:@"District heating systems, industrial systems" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"COOLFLEX"]) {
        //self.productImageView.image=[UIImage imageNamed:@"Coolflex.png"];
        [self addImageView:@"Coolflex.png"];
        self.applicationLabel.text =[Language get:@"District cooling systems, service and waste water" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"COOLMANT"]) {
         //self.productImageView.image=[UIImage imageNamed:@"COOLMANT.png"];
        [self addImageView:@"COOLMANT.png"];

        self.applicationLabel.text =[Language get:@"District cooling systems, service and waste water" alter:@"Tilte not found"];

    }
    else if ([productName isEqualToString:@"EIGERFLEX"]) {
        //self.productImageView.image=[UIImage imageNamed:@"Eigerflex.png"];
        [self addImageView:@"Eigerflex.png"];

        self.applicationLabel.text =[Language get:@"Cold- and waste water systems" alter:@"Tilte not found"];

    }
    else if ([productName isEqualToString:@"PREMANT"]) {
        //self.productImageView.image=[UIImage imageNamed:@"Premant.png"];
        [self addImageView:@"Premant.png"];

        self.applicationLabel.text =[Language get:@"District heating and cooling" alter:@"Tilte not found"];
    }
    
   
    
    else if ([productName isEqualToString:@"CALSTEEL"]) {
        //self.productImageView.image=[UIImage imageNamed:@"Calsteel.png"];
        [self addImageView:@"Calsteel.png"];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
    }

    else if ([productName isEqualToString:@"Stahlmantel-rohr"]) {
       // self.productImageView.image=[UIImage imageNamed:@"Stahlmantel.png"];
        [self addImageView:@"Stahlmantel.png"];
        self.applicationLabel.text =[Language get:@"Process heat systems" alter:@"Tilte not found"];
    }
    else if ([productName isEqualToString:@"PREMANTWickelfalz"]) {
        //self.productImageView.image=[UIImage imageNamed:@"PREMANT Wickelfalz.png"];
        [self addImageView:@"PREMANT Wickelfalz.png"];
        self.applicationLabel.text =[NSString stringWithFormat:@"District cooling and heating systems"];
    }
    else if ([productName isEqualToString:@"CALPEXSDR11"]) {
        //self.productImageView.image=[UIImage imageNamed:@""];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
        pipeImageArray = nil;
        pipeImageArray = [[NSMutableArray alloc]initWithObjects:@"CPX_Uno_rot.png",@"CPX-DUO-rot.png", @"CPX-Quad_rot.png",nil];
        [self addTableView];
    }
    else if ([productName isEqualToString:@"CALPEXSDR7.4"]) {
        //self.productImageView.image=[UIImage imageNamed:@""];
        self.applicationLabel.text =[Language get:@"District heating systems (house connections)" alter:@"Tilte not found"];
        pipeImageArray = nil;
        pipeImageArray = [[NSMutableArray alloc]initWithObjects:@"CPX_Uno_rot.png",@"CPX-DUO-rot.png", @"CPX-Quad_rot.png",nil];
        [self addTableView];

    }
    else if ([productName isEqualToString:@"CALUPEX"]) {
        //self.productImageView.image=[UIImage imageNamed:@"Calpex-ALU_DUO.png"];
        [self addImageView:@"Calpex-ALU_DUO.png"];
        self.applicationLabel.text =[Language get:@"District heating systems" alter:@"Tilte not found"];
    }

}

-(UIBarButtonItem *)addRightBarButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 20, 15)];
    //        [button setTitle:@"Share" forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"feedback"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(messageButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    return rightItem;
    
    
    
}
-(void)messageButtonClickedAction:(UIButton *)sender
{
//    FeddbackViewController *objFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeddbackViewController"];
//    
//    [self.navigationController pushViewController:objFeddbackViewController animated:YES];
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
  
     [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    
    [objMFMailcomposeVc setSubject:@""];
    
    //[objMFMailcomposeVc setToRecipients:[NSArray arrayWithObject:@""]];
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get:@"Alert" alter:@"Title not found"] message:[Language get: @"Please Signed in" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addImageView:(NSString *)imageName
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 480){
                // iPhone retina-3.5 inch
                
                
                UIView *tableBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,121,320,230)];//110//150
                //    tableBackgroundView.layer.contents = (id)[UIImage imageNamed:@"stars_of_the_day_bg"].CGImage;
                
                
               // tableBackgroundView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
                tableBackgroundView.backgroundColor = [UIColor whiteColor];
                
                [self.view addSubview:tableBackgroundView];
                
                // UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake(20,121, 250, 250)];
                UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake(45,15, 220, 200)];
                productImage.image = [UIImage imageNamed:imageName];
                //[productImage.layer setCornerRadius:10.0];
                //    [productImage setClipsToBounds:YES];
                //    [productImage.layer setMasksToBounds:YES];
                //    productImage.layer.borderColor = [UIColor whiteColor].CGColor;
                // productImage.layer.borderWidth = 1.0;
                productImage.backgroundColor = [UIColor whiteColor];
                [tableBackgroundView addSubview:productImage];

                
                
            } else{
                // iPhone retina-4 inch
              
                UIView *tableBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,121,320,290)];//110//150
                //    tableBackgroundView.layer.contents = (id)[UIImage imageNamed:@"stars_of_the_day_bg"].CGImage;
                
                
                //tableBackgroundView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
                 tableBackgroundView.backgroundColor = [UIColor whiteColor];
                
                
                [self.view addSubview:tableBackgroundView];
                
                // UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake(20,121, 250, 250)];
                UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake(30,15, 260, 260)];
                productImage.image = [UIImage imageNamed:imageName];
                //[productImage.layer setCornerRadius:10.0];
                //    [productImage setClipsToBounds:YES];
                //    [productImage.layer setMasksToBounds:YES];
                //    productImage.layer.borderColor = [UIColor whiteColor].CGColor;
                // productImage.layer.borderWidth = 1.0;
                productImage.backgroundColor = [UIColor whiteColor];
                [tableBackgroundView addSubview:productImage];

                
            }
        }
        else {
            // not retina display
        }
    }
    }
-(void)addTableView
{
    
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 480)
            {
                // iPhone retina-3.5 inch
                UIView *tableBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,121,320,230)];//110//150
                //    tableBackgroundView.layer.contents = (id)[UIImage imageNamed:@"stars_of_the_day_bg"].CGImage;
                
                
                
                //tableBackgroundView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
                tableBackgroundView.backgroundColor = [UIColor whiteColor];
                
                [self.view addSubview:tableBackgroundView];
                
                // Creating horizontal TableView
                
                UITableView *photosTableView;
                
                photosTableView                                = [[UITableView alloc] init];
                photosTableView.delegate                       = self;
                photosTableView.dataSource                     = self;
                photosTableView.rowHeight                      = 220;//150
                
                photosTableView.backgroundColor                = [UIColor whiteColor];
                photosTableView.separatorStyle                 = UITableViewCellSeparatorStyleNone;
                photosTableView.showsVerticalScrollIndicator   = NO;
                photosTableView.transform                      = CGAffineTransformRotate(CGAffineTransformIdentity, -90 * M_PI / 180.0);
                photosTableView.frame                          = CGRectMake(0, 10, 320, 230);//80//165
                [tableBackgroundView addSubview:photosTableView];
                
                
                // Creating a custom view above tableView
    
                
                
                
            } else{
                // iPhone retina-4 inch
                NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
                UIView *tableBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,121,320,290)];//110//150
                //    tableBackgroundView.layer.contents = (id)[UIImage imageNamed:@"stars_of_the_day_bg"].CGImage;
                
                
               // tableBackgroundView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
                 tableBackgroundView.backgroundColor = [UIColor whiteColor];
                
                [self.view addSubview:tableBackgroundView];
                
                // Creating horizontal TableView
                
                UITableView *photosTableView;
                
                photosTableView                                = [[UITableView alloc] init];
                photosTableView.delegate                       = self;
                photosTableView.dataSource                     = self;
                photosTableView.rowHeight                      = 270;//150
                
                photosTableView.backgroundColor                = [UIColor whiteColor];
                photosTableView.separatorStyle                 = UITableViewCellSeparatorStyleNone;
                photosTableView.showsVerticalScrollIndicator   = NO;
                photosTableView.transform                      = CGAffineTransformRotate(CGAffineTransformIdentity, -90 * M_PI / 180.0);
                photosTableView.frame                          = CGRectMake(0, 10, 320, 290);//80//165
                [tableBackgroundView addSubview:photosTableView];
                
                
                // Creating a custom view above tableView

                
                
            }
        }
        else {
            // not retina display
        }
    }
    
}
#pragma Mark-
#pragma mark - Table View Data SOurce
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [pipeImageArray count];//[starsOfDayArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIdentifier = @"Cell";
    DACustomTableCell *cell ;//= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil)
    {
        cell = [[DACustomTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:cellIdentifier] ;
    }
    else
    {
        for (UIView *view in cell.contentView.subviews) {
            
            if([view isKindOfClass:[UIImageView class]])
            {
                [view removeFromSuperview];
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *imgView;
    //cell.backgroundColor = [UIColor blueColor];
    //NSDictionary *userDictionary = [starsOfDayArray objectAtIndex:indexPath.section];
    
    // cell.photoImageView.showActivityIndicator = YES;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            if([UIScreen mainScreen].bounds.size.height == 480){
                // iPhone retina-3.5 inch
                imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 200, 200)];
                
            } else{
                // iPhone retina-4 inch
                imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 260, 260)];
                
                
            }
        }
        else {
            // not retina display
        }
    }
    
    
    //[imgView.layer setCornerRadius:10.0];
//    [imgView setClipsToBounds:YES];
//    [imgView.layer setMasksToBounds:YES];
//    imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    //imgView.layer.borderWidth = 1.0;
    imgView.backgroundColor = [UIColor whiteColor];
    
    // imgView.image = [UIImage imageNamed:@"aries.png"];
    [cell.contentView addSubview:imgView];
    imgView.image = [UIImage imageNamed:[pipeImageArray objectAtIndex:indexPath.section]];
    
//    if([appContext.loggedInUserGender isEqualToString:@"Male"])
//    {
//        [imgView setImageWithURL:[NSURL URLWithString:[userDictionary objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"female_dummy"]] ;
//    }
//    else
//    {
//        [imgView setImageWithURL:[NSURL URLWithString:[userDictionary objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"male_dummy"]] ;
//    }
    
    
    
    //cell.nameLabel.text = @"Lian";
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%@",error);
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
