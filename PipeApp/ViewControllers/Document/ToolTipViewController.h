//
//  ToolTipViewController.h
//  temp
//
//  Created by Shaikh Rizwan on 11/21/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"
#import <CoreLocation/CoreLocation.h>

@interface ToolTipViewController : UIViewController<CMPopTipViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate>

- (IBAction)seeImageButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *seeImgButton;

@end
