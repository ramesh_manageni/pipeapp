//
//  AudioRecorderViewController.h
//  PipeApp
//
//  Created by Redbytes on 08/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface AudioRecorderViewController : UIViewController<AVAudioPlayerDelegate,AVAudioRecorderDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *recordImageView;

@property (strong, nonatomic) IBOutlet UIButton *recordButon;

@property (strong, nonatomic) NSString *docRecordName;

@property (strong, nonatomic) UIImage *docImage;
@property (weak, nonatomic) IBOutlet UIImageView *recordImageIcon;

- (IBAction)recordButtonClicked:(id)sender;

- (IBAction)doneButtonClicked:(id)sender;


@end
