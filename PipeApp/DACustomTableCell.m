//
//  DACustomTableCell.m
//  DatingApp
//
//  Created by Chetan Zalake on 08/05/14.
//  Copyright (c) 2014 Chetan Zalake. All rights reserved.
//

#import "DACustomTableCell.h"

@implementation DACustomTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        self.backgroundColor=[UIColor clearColor];
        self.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 90 * M_PI / 180.0);
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    
}

@end
