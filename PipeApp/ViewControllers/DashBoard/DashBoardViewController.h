//
//  DashBoardViewController.h
//  PipeApp
//
//  Created by Redbytes on 25/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashBoardViewController : UIViewController
{
    
}

@property (weak, nonatomic) IBOutlet UIButton *productButton;
@property (weak, nonatomic) IBOutlet UIButton *productDocumentButton;
@property (weak, nonatomic) IBOutlet UIButton *calculatorButton;

@property (weak, nonatomic) IBOutlet UIButton *settingButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;


- (IBAction)productButtonClicked:(id)sender;

- (IBAction)documentButtonClicked:(id)sender;

- (IBAction)calculatorButtonClicked:(id)sender;

- (IBAction)settingButtonClicked:(id)sender;

- (IBAction)aboutButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end
