//
//  PipeRoughness.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PipeRoughness.h"


@implementation PipeRoughness

@dynamic pipeName;
@dynamic surfaceRoughness;

@end
