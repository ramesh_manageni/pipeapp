//
//  DocumentDetailViewController.h
//  PipeApp
//
//  Created by Redbytes on 07/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AudioRecorderViewController.h"
#import "Document.h"
#import "AudioRecorderViewController.h"
#import<MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import<MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"

@interface DocumentDetailViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVAudioPlayerDelegate,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate,MKMapViewDelegate>
{
    AudioRecorderViewController *objAudioRecorderViewController;
}


@property (strong, nonatomic) IBOutlet UITextField *docNameTextField;
@property (strong, nonatomic) IBOutlet UIView *docBaseView;
@property (strong, nonatomic) IBOutlet UIView *buttonBaseView;
@property (strong, nonatomic) IBOutlet UIImageView *docImageView;
@property (strong, nonatomic) IBOutlet UIButton *voiceRecordButton;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

- (IBAction)cameraButtonClicked:(id)sender;
- (IBAction)voiceButtonClicked:(id)sender;
- (IBAction)tooltipButtonClicked:(id)sender;
- (IBAction)shareButtonClicked:(id)sender;
- (IBAction)saveButtonClicked:(id)sender;
- (IBAction)voiceRecordButtonClicked:(id)sender;









@end
