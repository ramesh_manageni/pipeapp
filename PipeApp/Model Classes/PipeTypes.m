//
//  PipeTypes.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PipeTypes.h"
#import "PipeVariableDetail.h"


@implementation PipeTypes

@dynamic pipeName;
@dynamic pipeVariableDetail;

@end
