//
//  LoginViewController.m
//  PipeApp
//
//  Created by Redbytes on 08/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = @"Login";
    [self.loginButton.layer setCornerRadius:5.0];


}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = YES;

    
    
   
    //self.navigationItem.hidesBackButton = YES;
    self.passworTextField.text =@"";
    [self.passworTextField setPlaceholder:@"Password"];
    [self.tabBarController.tabBar setHidden:NO];
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"]) {
        
        
        
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        
        
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
//                                                  forKey:@"AppleLanguages"];
       // [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        
        
        
    }
    else
    {
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"de", nil] forKey:@"AppleLanguages"];
        
        
        
        
        //[[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        [Language setLanguage:@"de"];
        [self localizationAction];
        
    }
    
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    if ([password isEqualToString:@"Bru99"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}
-(void)localizationAction
{
    self.navigationItem.title=[Language get:@"Login" alter:@"title not found"];
    [self.loginButton setTitle:[Language get:@"Login" alter:@"title not found"] forState:UIControlStateNormal];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)loginButtonClicked:(id)sender
{
    if ([self.passworTextField.text isEqualToString:@"Bru99"] ) {
        
        [[NSUserDefaults standardUserDefaults] setObject:self.passworTextField.text forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        permission = YES;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
//    else if ([self.passworTextField.text isEqualToString:@"Bru99"] && [controllerTag isEqualToString:@"1"])
//    {
//        
//        [[NSUserDefaults standardUserDefaults] setObject:self.passworTextField.text forKey:@"password"];
//        
//        [[NSUserDefaults standardUserDefaults] synchronize];
//            AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
//           [appdelegte.objTab setSelectedIndex:1];
//        
//    }
//    else if ([self.passworTextField.text isEqualToString:@"Bru99"] && [controllerTag isEqualToString:@"3"])
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:self.passworTextField.text forKey:@"password"];
//        
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
//        [appdelegte.objTab setSelectedIndex:3];
//        
//    }
    else
    {
        permission = NO;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[Language get:@"Alert"alter:@"Title not found"] message:[Language get:@"Please enter Correct Password" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        [alert show];
        self.passworTextField.text =@"";
    }

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
       return YES;
    
}

@end
