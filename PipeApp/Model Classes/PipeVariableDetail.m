//
//  PipeVariableDetail.m
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "PipeVariableDetail.h"
#import "PipeTypes.h"


@implementation PipeVariableDetail

@dynamic pipeInner;
@dynamic pipeOuter;
@dynamic pipetype;
@dynamic pipewallThickness;
@dynamic pipeType;

@end
