//
//  SettingViewController.m
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "SettingViewController.h"
#import "Language.h"

@interface SettingViewController ()

@property(strong,nonatomic)NSMutableArray *settingArray;

@end

@implementation SettingViewController

@synthesize settingArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.settingArray=[[NSMutableArray alloc] initWithObjects:@"Language",@"Metric",@"Feedback",@"Disclaimer", nil];
    self.navigationItem.title = @"Settings";
    [self.logoutButton.layer setCornerRadius:5.0];

}
-(void)viewWillAppear:(BOOL)animated
{
    
//    [self.tabBarController.tabBar setHidden:NO];
//    [self.tableView reloadData];
    
    
    
    self.settingArray=nil;
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    // NSString * selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([savedValue isEqualToString:@"English"]) {
        // [Language setLanguage:@"en"];
        
        
        
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        
        
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
//                                                  forKey:@"AppleLanguages"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        
        [Language setLanguage:@"en"];
        
    }
    else
    {
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        
        //[Language setLanguage:@"de"];
//        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"de", nil] forKey:@"AppleLanguages"];
        
        
        
        
        //[[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    valueForKey:@"AppleLanguages"]);
        [Language setLanguage:@"de"];
        
    }
    
    
    
    //self.navigationItem.title = [Language get:@"Settings"alter:@"title not found"];
    
    
    //    NSString *language=[NSString stringWithFormat:@"%@:Language",savedValue];
    //    NSString *metric =[NSString stringWithFormat:@"%@:metric",savedValue];
    
    self.settingArray=[[NSMutableArray alloc] initWithObjects:[Language get:@"Language" alter:@"title not found"],[Language get:@"Metric" alter:@"title not found"],[Language get:@"Feedback" alter:@"title not found"],[Language get:@"Disclaimer" alter:@"title not found"],nil];
    
     self.navigationItem.title = [Language get:@"Settings" alter:@"title not found"];
    
    [self.logoutButton setTitle:[Language get:@"Logout" alter:@"title not found"] forState:UIControlStateNormal];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.tableView reloadData];
    [self.view endEditing:YES];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.settingArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if(indexPath.row!=0 && indexPath.row != 1)
        {
         [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
    }
    cell.textLabel.text=[self.settingArray objectAtIndex:indexPath.row];
    
    if(indexPath.row==0)
    {
        NSLog(@"AppleLanguages=%@",[[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"AppleLanguages"]);
        cell.detailTextLabel.text=[[NSUserDefaults standardUserDefaults]
                                   stringForKey:@"languageName"];
        
    }
    if(indexPath.row==1)
    {
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]
                     stringForKey:@"unitName"]);
        cell.detailTextLabel.text=[[NSUserDefaults standardUserDefaults]
                                   stringForKey:@"unitName"];
    }
    return cell;

    
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.00001;
}
#pragma mark UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.row==0)
    {
       languageViewController *objlanguageViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"languageViewController"];
        objlanguageViewController.isLanguageSelected=YES;
        
        [self.navigationController pushViewController:objlanguageViewController animated:YES];
        
    }
    if(indexPath.row==1)
    {
        languageViewController *objlanguageViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"languageViewController"];
        objlanguageViewController.isUnitSelected=YES;
        
        [self.navigationController pushViewController:objlanguageViewController animated:YES];
        
    }
    if(indexPath.row==2)
    {
//        FeddbackViewController *objFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeddbackViewController"];
//        
//        [self.navigationController pushViewController:objFeddbackViewController animated:YES];
        [self messageButtonClickedAction];
        
    }
    if(indexPath.row==3)
    {
        DisclaimerViewController *objDisclaimerViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DisclaimerViewController"];
        
        [self.navigationController pushViewController:objDisclaimerViewController animated:YES];
        
    }
    if(indexPath.row==4)
    {
        AboutViewController *objAboutViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutViewController"];
        
        [self.navigationController pushViewController:objAboutViewController animated:YES];
        
    }

}


- (IBAction)logoutButtonAction:(id)sender
{
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];

    if ([password isEqualToString:@"Bru99"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        password =@"";
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"Please Login First" alter:@"Title not found" ]delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
}
#pragma mark feedback method
-(void)messageButtonClickedAction
{
    //    FeddbackViewController *objFeddbackViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FeddbackViewController"];
    //
    //    [self.navigationController pushViewController:objFeddbackViewController animated:YES];
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    
    
    [objMFMailcomposeVc setSubject:@""];
    
    [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get:@"Alert" alter:@"Title not found"] message:[Language get:@"Please Signed in" alter:@"Title not found"] delegate:self cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        
        //alert.delegate=self;
        [alert show];
    }
    
    
    
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"%@",error);
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
