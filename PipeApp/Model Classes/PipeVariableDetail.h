//
//  PipeVariableDetail.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PipeTypes;

@interface PipeVariableDetail : NSManagedObject

@property (nonatomic, retain) NSNumber * pipeInner;
@property (nonatomic, retain) NSNumber * pipeOuter;
@property (nonatomic, retain) NSString * pipetype;
@property (nonatomic, retain) NSNumber * pipewallThickness;
@property (nonatomic, retain) PipeTypes *pipeType;

@end
