//
//  WarterVariableDetail.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Warter;

@interface WarterVariableDetail : NSManagedObject

@property (nonatomic, retain) NSNumber * density;
@property (nonatomic, retain) NSNumber * pressure;
@property (nonatomic, retain) NSNumber * temrature;
@property (nonatomic, retain) NSNumber * viscocity;
@property (nonatomic, retain) Warter *warter;

@end
