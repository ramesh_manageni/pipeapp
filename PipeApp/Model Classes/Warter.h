//
//  Warter.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 11/4/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WarterVariableDetail;

@interface Warter : NSManagedObject

@property (nonatomic, retain) NSString * tempInBar;
@property (nonatomic, retain) NSSet *warterVariableDetail;
@end

@interface Warter (CoreDataGeneratedAccessors)

- (void)addWarterVariableDetailObject:(WarterVariableDetail *)value;
- (void)removeWarterVariableDetailObject:(WarterVariableDetail *)value;
- (void)addWarterVariableDetail:(NSSet *)values;
- (void)removeWarterVariableDetail:(NSSet *)values;

@end
