//
//  ProductListViewController.m
//  PipeApp
//
//  Created by Redbytes on 21/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ProductListViewController.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"


@interface ProductListViewController ()

@property (nonatomic, strong) NSArray *contents;
@property BOOL isObservableTypeAvaible;

@end

@implementation ProductListViewController

@synthesize productImageArray,isObservableTypeAvaible;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tableView.SKSTableViewDelegate = self;
    self.productDetailLabel.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1];

    _contents=[self getContents];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self localizationAction];
    [self.navigationItem setTitle:[Language get:@"ability" alter:@"No title"]];
    [self.tabBarController.tabBar setHidden:NO];
    [self.tableView reloadData];
    NSString *temp = [NSString stringWithFormat:@"%@      %@     %@",temprature,bar,dimension];
    temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    NSLog(@"%@",temp);
    self.productDetailLabel.text = temp;
    [self.productDetailLabel setHidden:YES];
    
   
    ///////////
    UILabel *productLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, self.view.frame.size.width, 35)];
    productLabel.backgroundColor = [UIColor whiteColor];
    productLabel.text = temp;//Bitte gewünschten Wert auswählen
    productLabel.numberOfLines = 2;
    productLabel.layer.borderColor = [UIColor blackColor].CGColor;
    productLabel.layer.borderWidth = 1.0;
    productLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:productLabel];
    ///////
}
-(void)localizationAction
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    if ([savedValue isEqualToString:@"Deutsch"]) {
        [Language setLanguage:@"de"];
        
    }
    else{
        [Language setLanguage:@"en"];
        
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSArray *)getContents
{
    NSMutableArray *mainArray=[[NSMutableArray alloc] init];
    NSMutableArray *superArray=[[NSMutableArray alloc] init];
    
    NSMutableArray *subArray=[[NSMutableArray alloc] init];
    NSMutableArray *subArray1=[[NSMutableArray alloc] init];
    
    
    
    for(int i=0;i<self.productImageArray.count;i++)
    {
        if([[[productImageArray objectAtIndex:i] valueForKey:@"observableId"] intValue]==2)
        {
            isObservableTypeAvaible=NO;
        }
        else
        {
            isObservableTypeAvaible=YES;
            
            [subArray addObject:@"Observable"];
            [subArray1 addObject:@"Non Observable"];
            
            break;
            
        }
        
    }
    
    for(int i=0;i<self.productImageArray.count;i++)
    {
        
        if([[[productImageArray objectAtIndex:i] valueForKey:@"observableId"] intValue]==1)
        {
            [subArray addObject:[[productImageArray objectAtIndex:i] valueForKey:@"productName"]];
            
        }
        else if ([[[productImageArray objectAtIndex:i] valueForKey:@"observableId"] intValue]==0)
        {
            [subArray1 addObject:[[productImageArray objectAtIndex:i] valueForKey:@"productName"]];
            
        }
    }
    
    for(int i=0;i<self.productImageArray.count;i++)
    {
        
        if([[[productImageArray objectAtIndex:i] valueForKey:@"observableId"] intValue]==2)
        {
            NSMutableArray *subArray=[[NSMutableArray alloc] init];

            [subArray addObject:[[productImageArray objectAtIndex:i] valueForKey:@"productName"]];
            
            [mainArray addObject:subArray];
            
            subArray=nil;

            
        }
    }

    
if(isObservableTypeAvaible)
{
    if(subArray.count>1)
    {
        [mainArray addObject:subArray];
        
    }
    if(subArray1.count>1)
    {
        [mainArray addObject:subArray1];
        
    }
}


subArray=nil;
subArray1=nil;

[superArray addObject:mainArray];

NSLog(@"%@",superArray);


//    if (!_contents)
//    {
//        _contents = @[
//                      @[
//                          @[@"Section0_Row0", @"Row0_Subrow1",@"Row0_Subrow2"],
//                          @[@"Section0_Row1", @"Row1_Subrow1", @"Row1_Subrow2", @"Row1_Subrow3", @"Row1_Subrow4", @"Row1_Subrow5", @"Row1_Subrow6", @"Row1_Subrow7", @"Row1_Subrow8", @"Row1_Subrow9", @"Row1_Subrow10", @"Row1_Subrow11", @"Row1_Subrow12"],
//                          @[@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2",@"Section0_Row2"]],
//
//                      ];
//    }

return superArray;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.contents count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contents[section] count];
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.contents[indexPath.section][indexPath.row] count] - 1;
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        return YES;
    }
    
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    //    NSData *data = [self.contents[indexPath.section][indexPath.row][0] dataUsingEncoding:NSUTF8StringEncoding];
    //    NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    if ([@"Observable" isEqualToString:self.contents[indexPath.section][indexPath.row][0]]) {
        cell.textLabel.text = [Language get:@"Observable" alter:@"Title not found"];
    }
   else if ([@"Non Observable" isEqualToString:self.contents[indexPath.section][indexPath.row][0]]) {
        cell.textLabel.text = [Language get:@"Non Observable" alter:@"Title not found"];
    }
    else
    {
        cell.textLabel.text = self.contents[indexPath.section][indexPath.row][0];
    }
    NSLog(@"%@",self.contents[indexPath.section][indexPath.row][0]);
    
   // cell.textLabel.text = self.contents[indexPath.section][indexPath.row][0];
    
    //    if ((indexPath.section == 0 && (indexPath.row == 1 || indexPath.row == 0)) || (indexPath.section == 1 && (indexPath.row == 0 || indexPath.row == 2)))
    //        cell.expandable = YES;
    //    else
    
    if(isObservableTypeAvaible)
    {
        cell.expandable = YES;

    }
    else
    {
        cell.expandable = NO;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    //    NSData *data = [self.contents[indexPath.section][indexPath.row][indexPath.subRow] dataUsingEncoding:NSUTF8StringEncoding];
    //    NSString *decodevalue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    //
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", self.contents[indexPath.section][indexPath.row][indexPath.subRow]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSLog(@"Section: %d, Row:%d, Subrow:%d", indexPath.section, indexPath.row, indexPath.subRow);
   // observType = self.contents[indexPath.section][indexPath.row][0];
    //NSLog(@"observType: %@", observType);
    
    if(!isObservableTypeAvaible)
    {
        ProductDetailsViewController *objProductDetailsViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        observType = nil;
        
        objProductDetailsViewController.productName = self.contents[indexPath.section][indexPath.row][0];
        
        [self.navigationController pushViewController:objProductDetailsViewController animated:YES];
        

    }
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    
    ProductDetailsViewController *objProductDetailsViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    
    objProductDetailsViewController.productName = self.contents[indexPath.section][indexPath.row][indexPath.subRow];
    observType = self.contents[indexPath.section][indexPath.row][0];
    [self.navigationController pushViewController:objProductDetailsViewController animated:YES];
    

    
}

#pragma mark - Actions




- (void)reloadTableViewWithData:(NSArray *)array
{
    self.contents = array;
    
    // Refresh data not scrolling
    //    [self.tableView refreshData];
    
    [self.tableView refreshDataWithScrollingToIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
}




@end
