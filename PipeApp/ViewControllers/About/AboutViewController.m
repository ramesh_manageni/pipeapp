//
//  AboutViewController.m
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AboutViewController.h"
#import "ViewAnnotion.h"

@interface AboutViewController ()
@property (nonatomic) CLLocationManager *locationManager;
@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    //self.navigationItem.title = @"About";
    self.mapView.delegate = self;
    [self.mapView setMapType: MKMapTypeStandard];
    _locationManager = [[CLLocationManager alloc]init];
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES
    ;

    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.phoneLabel addGestureRecognizer:tapRecognizer];
   
       // *code for showing region zoom in which company location show directly*
    
    
//     MKCoordinateRegion region;
//    region.center.latitude = 47.574435;
//    region.center.longitude = 8.235995;
//    
//    ViewAnnotion *objViewAnnotion= [[ViewAnnotion alloc]init];
//    objViewAnnotion.title=@"hello";
//    objViewAnnotion.subtitle=@"Hi";
//    objViewAnnotion.coordinate= region.center;
//    self.mapView.showsUserLocation=YES;
//    
//    [self.mapView addAnnotation:objViewAnnotion];
//    [self.mapView setRegion:region animated:YES];
    
    
    /////////////////////////////////////
    
    
    
    // * code for zoom in seted region
    MKCoordinateRegion region;
    region.center.latitude = 47.574435;
    region.center.longitude = 8.235995;
    MKCoordinateSpan span = MKCoordinateSpanMake(0.03, 0.08);//(0.05, 0.10)
    MKCoordinateRegion region1 = MKCoordinateRegionMake(region.center, span);
    ViewAnnotion *objViewAnnotion= [[ViewAnnotion alloc]init];
    objViewAnnotion.title=@"hello";
    objViewAnnotion.subtitle=@"Hi";
    objViewAnnotion.coordinate= region1.center;
    self.mapView.showsUserLocation=YES;
    
    [self.mapView addAnnotation:objViewAnnotion];
    [self.mapView setRegion:region1 animated:YES];
    [self.mapView regionThatFits:region1];
    /////////////////////////
    
    self.siteLabel.text=@"www.pipesystems.com";//@"pipesystems@brugg.com";
    
    self.siteTextview.text =@"www.pipesystems.com";//@"http://www.bruggpipesystems.com" ;//@"http://pipesystems@brugg.com";
    self.siteTextview.editable = NO;
   self.siteTextview.dataDetectorTypes = UIDataDetectorTypeLink;
    //cell is the TableView's cell
//    [self.objScrollView addSubview:myView];


}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:NO];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    // NSString * selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([savedValue isEqualToString:@"English"])
    {
        [Language setLanguage:@"en"];
        
    }
    else
    {
        
        [Language setLanguage:@"de"];
        
    }
self.navigationItem.title = [Language get:@"About" alter:@"Title not found"];
    
}
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    NSString *number = [[self.phoneLabel.text componentsSeparatedByString:@":"] objectAtIndex:1];
    NSString *phoneNumber = [@"tel://" stringByAppendingString:@"41562687878"];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    // Current Location
//    CLLocation *currentLocation = [locations objectAtIndex:0];
//    //currentLocation.coordinate.latitude=47.574435;
//    NSLog(@"currentLocation=%@",currentLocation);
//    
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = currentLocation.coordinate;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [self.mapView addAnnotation:point];
//    
//    
//    
//   
//}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    
    
    //code for showing location with zoomout
    
//    MKCoordinateRegion region;
//    region.center.latitude = 47.574435;
//    region.center.longitude = 8.235995;
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
//
//
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = userLocation.coordinate;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [self.mapView addAnnotation:point];
}


@end
