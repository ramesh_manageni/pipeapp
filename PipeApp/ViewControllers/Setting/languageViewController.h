//
//  languageViewController.h
//  PipeApp
//
//  Created by Redbytes on 30/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"AppDelegate.h"
@interface languageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *languageTableview;


@property BOOL isLanguageSelected;
@property BOOL isUnitSelected;

@end
