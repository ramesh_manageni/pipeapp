//
//  SettingViewController.h
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<MessageUI/MessageUI.h>
#import "languageViewController.h"
#import "FeddbackViewController.h"
#import "DisclaimerViewController.h"
#import "AboutViewController.h"

@interface SettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
- (IBAction)logoutButtonAction:(id)sender;
@end
