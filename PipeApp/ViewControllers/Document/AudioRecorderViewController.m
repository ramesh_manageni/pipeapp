//
//  AudioRecorderViewController.m
//  PipeApp
//
//  Created by Redbytes on 08/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "AudioRecorderViewController.h"
#import "AppDelegate.h"
#include <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"
@interface AudioRecorderViewController ()

{
    
    AVAudioRecorder *recorder;
    
    AVAudioPlayer *player;
    AppDelegate *delegate;
    MPMoviePlaybackState playerState;
    int count;
    
    NSURL *outputFileURL;

}

@end

@implementation AudioRecorderViewController

@synthesize docRecordName,docImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.recordImageView.image=docImage;
    self.recordImageIcon.image = [UIImage imageNamed:@""];

    delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
     [self.recordButon.layer setCornerRadius:5.0];

     //outputFileURL =  [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:@"MyAudioMemo.caf"];
  
    

    
}
-(void)barbuttonMethod
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"])
        {
            [Language setLanguage:@"en"];
        }
        
        else
        {
            [Language setLanguage:@"de"];
        }

    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[Language get:@"Back" alter:@"title not found"] style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[Language get:@"Done" alter:@"title not found"] style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonClicked:)];
    // self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    [self.recordButon setTitle:[Language get:@"Record" alter:@"title not found"] forState:UIControlStateNormal];
    self.navigationItem.title = [Language get:@"Documents" alter:@"title not found"];//@"Documents";

}

-(void)viewWillAppear:(BOOL)animated
{    [self.recordButon setEnabled:YES];
    [self barbuttonMethod];
    outputFileURL =  [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:docRecordName];

//    // Setup audio session
//    AVAudioSession *session = [AVAudioSession sharedInstance];
//    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//    
//    // Define the recorder setting
//    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
//    
//    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
//    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
//    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
//    
//    // Initiate and prepare the recorder
//    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
//    recorder.delegate = self;
//    recorder.meteringEnabled = YES;
//    //    [recorder prepareToRecord];

}
-(void)back
{
    [self removeVoiceRecord:docRecordName];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (void)removeVoiceRecord:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    
    isVoiceRecordNotDone=YES;
    
    //    if (success) {
//        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
//        [removeSuccessFulAlert show];
//    }
//    else
//    {
//        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
//    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)recordButtonClicked:(id)sender
{
    [self.recordButon setEnabled:NO];
    self.recordImageIcon.image = [UIImage imageNamed:@"record.jpeg"];
//    [recorder prepareToRecord];
//    if (!recorder.recording)
//    {
//   
//        AVAudioSession *session = [AVAudioSession sharedInstance];
//        [session setActive:YES error:nil];
//        // Start recording
//        [recorder record];
//        
//    }
    
    
    
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    nil];
    NSError *error = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSettings error:&error];
    recorder.delegate=self;
    [recorder prepareToRecord];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryRecord error:nil];
    [session setActive:YES error:nil];
    
    [recorder record];

}
- (IBAction)doneButtonClicked:(id)sender
{
//    [recorder stop];
//
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    [audioSession setActive:NO error:nil];
    
    [recorder stop];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    int flags = AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation;
    [session setActive:NO withOptions:flags error:nil];
    isVoiceRecordNotDone=NO;

    [self.navigationController popViewControllerAnimated:YES];
/*
//    if (!player)
//    {
//        player = [[AVAudioPlayer alloc] initWithContentsOfURL:outputFileURL error:nil];
//        NSLog(@"Urlpath = %@", outputFileURL);
//        [player setDelegate:self];
//        [player prepareToPlay];
//    }
//    [player play];
//*/
}

#pragma mark - AVAudioRecorder Delegate MEthods

/* audioRecorderDidFinishRecording:successfully: is called when a recording has been finished or stopped. This method is NOT called if the recorder is stopped due to an interruption. */
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    NSLog(@"In %s",__PRETTY_FUNCTION__);
}

/* if an error occurs while encoding it will be reported to the delegate. */
- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    NSLog(@"In %s",__PRETTY_FUNCTION__);
    NSLog(@"audioRecorderEncodeError:- %@",[error localizedDescription]);
}
-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}
-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}


#pragma mark - AVAudioPlayer Delegate MEthods

/* audioPlayerDidFinishPlaying:successfully: is called when a sound has finished playing. This method is NOT called if the player is stopped due to an interruption. */
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"In %s",__PRETTY_FUNCTION__);
    
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"In %s",__PRETTY_FUNCTION__);
    NSLog(@"audioPlayerDecodeError:- %@",[error localizedDescription]);
}

@end
