//
//  DocumentViewController.h
//  PipeApp
//
//  Created by Redbytes on 29/09/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentDetailViewController.h"
#import "Document.h"
#import "ShowDocumentViewController.h"
#import"LoginViewController.h"

@interface DocumentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *DocTableView;
- (IBAction)addDocButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addDocumentbutton;

@end
