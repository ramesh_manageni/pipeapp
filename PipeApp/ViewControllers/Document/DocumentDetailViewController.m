//
//  DocumentDetailViewController.m
//  PipeApp
//
//  Created by Redbytes on 07/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "DocumentDetailViewController.h"
#import "ToolTipViewController.h"

@interface DocumentDetailViewController ()
{
    AVAudioPlayer *player;
     NSURL *outputFileURL;
    CGRect oldFrame;
   CLLocationManager *_locationManager;
}


@property (nonatomic, strong)NSMutableDictionary *recordDict;

@end

@implementation DocumentDetailViewController


@synthesize recordDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    oldFrame = [self.saveButton frame];
    [super viewDidLoad];
   DocumentImage = [UIImage imageNamed:@"Noimage"];//Noimage
	// Do any additional setup after loading the view.
//    [self.docBaseView.layer setCornerRadius:5.0];
//    [self.docBaseView.layer setBorderWidth:1.0];
    [self.buttonBaseView.layer setBorderWidth:1.0];
    
    [self.voiceRecordButton.layer setCornerRadius:5.0];
    [self.voiceRecordButton.layer setBorderWidth:1.0];
    
    [self.docImageView.layer setCornerRadius:5.0];
    [self.docImageView.layer setBorderWidth:1.0];

   
    [self.saveButton.layer setCornerRadius:5.0];
    
//    [self.buttonBaseView.layer setBorderColor:CGColorRetain([UIColor colorWithRed:2.0 green:1.0
//                                                                             blue:1.0 alpha:1.0].CGColor)];
//
    
    //Dictionary used to store(temporary) address information
    recordDict=[[NSMutableDictionary alloc] init];
    isVoiceRecordNotDone=YES;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 60, 30)];
    [backButton setTitle:[Language get:@"Back" alter:@"No"] forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor colorWithRed:12/255.0 green:95/255.0 blue:254/255.0 alpha:1] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backbuttonAction) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftItem;
    
}
-(void)backbuttonAction
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"Save the document"  alter:@"Title 1"]delegate:self cancelButtonTitle:[Language get: @"Yes" alter:@"Title not found"] otherButtonTitles:[Language get: @"No" alter:@"Title not found"], nil];
    [alert show];
}
-(void)viewWillAppear:(BOOL)animated
{
    
     [self.tabBarController.tabBar setHidden:NO];
    
    self.docImageView.image = DocumentImage;
    //DocumentImage = [UIImage imageNamed:@"Noimage"];
//    [self.view setAutoresizingMask:UIViewAutoresizingNone];

    if(isVoiceRecordNotDone)
    {
        //[self.voiceRecordButton removeFromSuperview];
        
        [self.voiceRecordButton setTitle:[NSString stringWithFormat:@"  No voice record available  "] forState:UIControlStateNormal];
        [self.voiceRecordButton setEnabled:NO];

    }
    else
    {
//        CGRect frame = [self.voiceRecordButton frame];
//        frame.size.width = 150;
//        
//        [self.voiceRecordButton setFrame:frame];
        [self.voiceRecordButton setEnabled:YES];

        [self.docBaseView addSubview:self.voiceRecordButton];
        self.voiceRecordButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.voiceRecordButton setTitle:[NSString stringWithFormat:@"  %@  \n \n          Play ",objAudioRecorderViewController.docRecordName] forState:UIControlStateNormal];//
    }
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"]) {
        
       
        
        [Language setLanguage:@"en"];
        [self localizationAction];
        
        //        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil]
        //                                                  forKey:@"AppleLanguages"];
        // [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        
        
    }
    else
    {
        //        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"de", nil] forKey:@"AppleLanguages"];
        //
        //
        //
        //
        //       [[NSUserDefaults standardUserDefaults] synchronize];
        
        [Language setLanguage:@"de"];
        
        //self.navigationItem.title=[Language get:@"Documents" alter:@"title not found"];
        [self localizationAction];
        
    }


}
-(void)localizationAction
{
     self.navigationItem.title = [Language get:@"Documents" alter:@"Title not found"];
    [self.saveButton setTitle:[Language get:@"Save Documentation" alter:@"Title not found"] forState:UIControlStateNormal];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraButtonClicked:(id)sender
{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"languageName"];
    
    if ([savedValue isEqualToString:@"English"]) {
        [Language setLanguage:@"en"];
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Camera Roll",
//                            @"Take a Picture",
//                            nil];
//    popup.tag = 1;
//    [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else
    {
        [Language setLanguage:@"de"];
        
    }
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:[Language get:@"Select Picture"  alter:@"Title not found"]delegate:self cancelButtonTitle:[Language get:@"Cancel"  alter:@"Title not found"] destructiveButtonTitle:nil otherButtonTitles:
                            [Language get:@"Camera Roll" alter:@"Title not found"] ,
                            [Language get:@"Take a picture" alter:@"Title not found"],
                            nil];
    popup.tag = 1;
    //[popup showInView:[UIApplication sharedApplication].keyWindow];
    [popup showInView:self.view];
}

- (IBAction)voiceButtonClicked:(id)sender
{
      objAudioRecorderViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AudioRecorderViewController"];
    objAudioRecorderViewController.docImage=self.docImageView.image;
    DocumentImage = self.docImageView.image;
    
    [self saveDocVoiceRecordToDocDirectory];
    
    [self.navigationController pushViewController:objAudioRecorderViewController animated:YES];
    

}

- (IBAction)tooltipButtonClicked:(id)sender
{
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ToolTipViewController  *objToolTipViewController =[main instantiateViewControllerWithIdentifier:@"ToolTipViewController"];
    DocumentImage = self.docImageView.image;
    
    [self.navigationController pushViewController:objToolTipViewController animated:YES];

}


- (IBAction)shareButtonClicked:(id)sender {
    if ([self.docNameTextField.text length]>0) {
    
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
   
        UIImage *emailImage;
    
    emailImage = self.docImageView.image;
    
        NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
        
        [objMFMailcomposeVc  addAttachmentData:imageData
                                      mimeType:@"image/png"
                                      fileName:@"Foto.png"];
    
    
    if (objAudioRecorderViewController.docRecordName!=nil) {
        NSURL *audioURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:objAudioRecorderViewController.docRecordName];
        
        NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
        
        NSLog(@"DATA %@ ",audioData);
        
        [objMFMailcomposeVc addAttachmentData:audioData mimeType:@"audio/caf" fileName:[NSString stringWithFormat:@"%@",objAudioRecorderViewController.docRecordName]];
    }

    
   
    

    
    // Email Subject
    //NSString *emailTitle = @"Auftrag Detailliertheit";
    NSString *emailTitle =[NSString stringWithFormat:@"Document"];
    
    // Email Content
    
    // To address
    [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    
    [objMFMailcomposeVc setSubject:emailTitle];
    [objMFMailcomposeVc setMessageBody:self.docNameTextField.text isHTML:YES];
    // [objMFMailcomposeVc setToRecipients:toRecipents];
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
        //        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
        //        
        //alert.delegate=self;
        //[alert show];
    }
    }
    else{
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get: @"Please enter document name" alter:@"Title"] delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
//        [alert show];
         NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"languageName"];
        if ([savedValue isEqualToString:@"Deutsch"]) {
            [Language setLanguage:@"de"];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:@"Bitte geben Sie Name des Dokuments" delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else
        {
            
            [Language setLanguage:@"en"];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:@"Please enter document name"  delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        


    }
}



- (IBAction)saveButtonClicked:(id)sender
{
NSString *temp = [self.docNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    BOOL success = NO;
    if (temp.length>0) {
        success=[self stringCheker:temp];
    }
    if (success==YES) {
      
    if(temp.length>0 && self.docNameTextField.text.length>0 && self.docNameTextField.text.length<31)
    {
//        if ([self.docNameTextField.text length] <50) {
//            <#statements#>
//        }
    [recordDict setValue:self.docNameTextField.text forKey:@"docName"];
    [self saveDocImageToDocDirectory];
    [self insertRecord];
        DocumentImage = [UIImage imageNamed:@""];
    [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        
        //alert on  text length >30
        if (self.docNameTextField.text.length>30) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"Document Name is too long"  alter:@"Title 1"]delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            //doc name is not enter or contain only blank character
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"Please enter document name" alter:@"Title 2"] delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        [alert show];
        }
    }
    }
    else{
        //alert for doc contain special symbol at start
        
        if (temp.length==0)
        {
            NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"languageName"];
            if ([savedValue isEqualToString:@"Deutsch"]) {
                [Language setLanguage:@"de"];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:@"Bitte geben Sie Name des Dokuments" delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
                [alert show];

                
            }
            else{
                
                [Language setLanguage:@"en"];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:@"Please enter document name"  delegate:nil cancelButtonTitle:[Language get: @"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
                [alert show];

                
            }

            
                    }
        else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[Language get: @"Alert" alter:@"Title not found"] message:[Language get:@"Special character are not allowed" alter:@"Title 4"] delegate:nil cancelButtonTitle:[Language get:@"Ok" alter:@"Title not found"] otherButtonTitles:nil, nil];
        [alert show];
        }

    }

}

- (IBAction)voiceRecordButtonClicked:(id)sender
{
             outputFileURL =  [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:objAudioRecorderViewController.docRecordName];
         
//         player = [[AVAudioPlayer alloc] initWithContentsOfURL:outputFileURL error:nil];
//         NSLog(@"Urlpath = %@", outputFileURL);
//         [player setDelegate:self];
//         [player prepareToPlay];
//     
//     [player play];

    NSError *error;
   player = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:outputFileURL
                        error:&error];
    player.delegate = self;
    
    [player prepareToPlay];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:YES error:nil];
    [player play];
}
-(BOOL)stringCheker:(NSString *)string
{
    char a = [string characterAtIndex:0];
    switch (a) {
        case '*':
            return NO;
            break;
        case '!':
            return NO;
            break;
        case '@':
            return NO;
            break;

        case '#':
            return NO;
            break;
        case '$':
            return NO;
            break;
        case '%':
            return NO;
            break;

        case '^':
            return NO;
            break;
        case '&':
            return NO;
            break;

        case '(':
            return NO;
            break;
        case ')':
            return NO;
            break;
            
        case '+':
            return NO;
            break;
        case '-':
            return NO;
            break;
        case '?':
            return NO;
            break;

        default:
            break;
    }
        return YES;
}

# pragma mark UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//   if(IS_IPHONE_5)
//   {
//       [UIView animateWithDuration:0.399 animations:^{self.buttonBaseView.frame = CGRectMake(0, 310, self.buttonBaseView.frame.size.width, self.buttonBaseView.frame.size.height);}];
//
//       
//   }
//    else
//    {
//        [UIView animateWithDuration:0.399 animations:^{self.buttonBaseView.frame = CGRectMake(0, 220, self.buttonBaseView.frame.size.width, self.buttonBaseView.frame.size.height);}];
//  
//    }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    if(IS_IPHONE_5)
    {
        [UIView animateWithDuration:0.25 animations:^{self.buttonBaseView.frame = CGRectMake(0, 339, self.buttonBaseView.frame.size.width, self.buttonBaseView.frame.size.height);}];
 
    }
    else
    {

    [UIView animateWithDuration:0.25 animations:^{self.buttonBaseView.frame = CGRectMake(0, 339, self.buttonBaseView.frame.size.width, self.buttonBaseView.frame.size.height);}];
    }

    return YES;

}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    
//    switch (popup.tag)
//    {
//        case 1:
//        {
//            switch (buttonIndex)
//            {
//                case 0:
//                    
//                    [self importPhotoButtonClicked];
//                    break;
//                case 1:
//                    [self takePhotoButtonClicked];
//                    break;
//                default:
//                    break;
//            }
//            break;
//        }
//        default:
//            break;
//    }
    
    if (buttonIndex == 0) {
        UIImagePickerControllerSourceType sourceType= UIImagePickerControllerSourceTypePhotoLibrary;

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self openPhotoPicker:sourceType];
        }];
    }
    else if (buttonIndex == 1)
    {
        UIImagePickerControllerSourceType sourceType= UIImagePickerControllerSourceTypeCamera;

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self openPhotoPicker:sourceType];
        }];
    }
}

- (void)takePhotoButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Fehler"
                                                              message:@"Gerät hat keine Kamera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"akzeptieren"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}
// use photo from PhotoLibrary
- (void)importPhotoButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

#pragma mark UIImagePickerControllerDelegate
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{ NSData *imageData;
    
    if (image != nil) {
        UIGraphicsBeginImageContext(CGSizeMake(640, 960));
        [image drawInRect: CGRectMake(0, 0, 640, 960)];
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        imageData = UIImageJPEGRepresentation(smallImage, 0.05f);
        //imageData = [theImage getData];
        self.docImageView.image = [UIImage imageWithData:imageData];
        DocumentImage = [UIImage imageWithData:imageData];
            }
      
//    self.docImageView.image=image;
//    DocumentImage = image;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Database Method

-(void)saveDocImageToDocDirectory
{
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int num;
    num=0;
    
    do
    {
        NSString  *savedImagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Foto-%d.png",num]];
        num += 1; // for next time
        
        if ( ![fm fileExistsAtPath: savedImagePath] )
        {
            // save your image here using savedImagePath
            [recordDict setValue:savedImagePath forKey:@"imageURL"];
            
           // NSData* data = UIImagePNGRepresentation(self.docImageView.image);
            NSData* data = UIImagePNGRepresentation(DocumentImage);
            [data writeToFile:savedImagePath atomically:YES];
            
            break;
        }
        
    } while (num);
    
    
}
-(void)saveDocVoiceRecordToDocDirectory
{
    NSFileManager *fm = [[NSFileManager alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    int num;
    num=0;
    
    do
    {
        NSString  *savedVoiceRecordPath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"VoiceRecord-%d.aac",num]];
        
        if ( ![fm fileExistsAtPath: savedVoiceRecordPath] )
        {
            // save your image here using savedImagePath
            [recordDict setValue:savedVoiceRecordPath forKey:@"voiceRecordURL"];
            
            objAudioRecorderViewController.docRecordName=[NSString stringWithFormat:@"VoiceRecord-%d.aac",num];
            
            //            NSData* data = UIImagePNGRepresentation(self.docImageView.image);
//            [data writeToFile:savedImagePath atomically:YES];
//            
            break;
        }
        num += 1; // for next time

    } while (num);
    
}


//Add address information to the database
-(void)insertRecord
{
    NSArray *stringArray = [[self getCurrentDateTime] componentsSeparatedByString:@"-"];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    Document *recordNew;
    
//    if(record!=nil)
//    {
//        recordNew=(PhotoDetail *)record;
//        record=nil;
//    }
//    else
//    {
    
        recordNew=[NSEntityDescription insertNewObjectForEntityForName:@"Document" inManagedObjectContext:context];
        
   // }
    if(![[recordDict valueForKey:@"docName"] isEqual:[NSNull null]])
    {
        recordNew.docName=[recordDict valueForKey:@"docName"];
    }
    if(![[recordDict valueForKey:@"imageURL"] isEqual:[NSNull null]])
    {
        recordNew.docImageName=[recordDict valueForKey:@"imageURL"];
    }
    if(![[recordDict valueForKey:@"voiceRecordURL"] isEqual:[NSNull null]])
    {
        recordNew.docRecordingName=[recordDict valueForKey:@"voiceRecordURL"];
    }
    
    if(stringArray.count >1)
    {
        //recordNew.do = [NSString stringWithFormat:@"%@",[stringArray objectAtIndex:0]];
        NSLog(@"%@",[stringArray objectAtIndex:0]);
    
        recordNew.docDate = [NSString stringWithFormat:@"%@",[stringArray objectAtIndex:0]];
        recordNew.docTime = [NSString stringWithFormat:@"%@",[stringArray objectAtIndex:1]];
        
    }
    
    
      NSError *err;
    
    if( ! [context save:&err] )
    {
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    
}

-(NSString *)getCurrentDateTime
{
    NSDate* currentDate = [NSDate date];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd.MM.yyyy-HH.mm"];
    NSString *dateInString = [dateFormatter stringFromDate:currentDate];
    
    NSLog(@"dateInString = %@",dateInString);
    return dateInString;
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{// [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
            
        case MFMailComposeResultSent:
        {
            
            NSLog(@"Mail sent");
            break;
        }
            
        case MFMailComposeResultFailed:
            
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            
        default:
            break;
            
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)openPhotoPicker:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imgpicker = [[UIImagePickerController alloc]init];
    
    imgpicker.delegate = self;
    imgpicker.sourceType = sourceType;
    
    [self.navigationController presentViewController:imgpicker animated:YES completion:nil];
    
    
}

#pragma mark Alert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *temp = [self.docNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(buttonIndex == 0)
    {
        if (temp.length>0)
        {
            [self saveButtonClicked:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
            [self.docNameTextField becomeFirstResponder];
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
}
@end
