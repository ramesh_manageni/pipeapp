//
//  ShowDocumentViewController.m
//  PipeApp
//
//  Created by Redbytes on 10/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import "ShowDocumentViewController.h"

@interface ShowDocumentViewController ()
{
   NSURL *outputFileURL;
    AVAudioPlayer *player;

}
@end

@implementation ShowDocumentViewController

@synthesize docRecord;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.docBaseView.layer setCornerRadius:5.0];
   // [self.docBaseView.layer setBorderWidth:.5];
    [self.buttonBaseView.layer setBorderWidth:0.5];
    
    //[self.voiceRecordButton removeFromSuperview];
    
    NSLog(@"%@",docRecord);
    
    BOOL fileExists=[[NSFileManager defaultManager] fileExistsAtPath:[docRecord valueForKey:@"docImageName"]];
    
    if (fileExists)
    {
        self.docImageView.image=[UIImage imageWithContentsOfFile:[docRecord valueForKey:@"docImageName"]];
    }

    BOOL fileExistsVoice=[[NSFileManager defaultManager] fileExistsAtPath:[docRecord valueForKey:@"docRecordingName"]];
    
    if (fileExistsVoice)
    {
        [self.docBaseView addSubview:self.voiceRecordButton];
       // self.voiceRecordButton.titleLabel.text=[[docRecord valueForKey:@"docRecordingName"] lastPathComponent];
        [self.voiceRecordButton setTitle:[NSString stringWithFormat:@"  %@  ",[[docRecord valueForKey:@"docRecordingName"] lastPathComponent]] forState:UIControlStateNormal];
        [self.voiceRecordButton.layer setCornerRadius:5.0];
        [self.voiceRecordButton.layer setBorderWidth:1.0];
        [self.voiceRecordButton setEnabled:YES];


    }
    else
    {
        [self.voiceRecordButton setTitle:[NSString stringWithFormat:@"  No voice record available  "] forState:UIControlStateNormal];
        [self.voiceRecordButton.layer setCornerRadius:5.0];
        [self.voiceRecordButton.layer setBorderWidth:1.0];

        [self.voiceRecordButton setEnabled:NO];
    }
    
    if([docRecord valueForKey:@"docName"]!=nil)
    {
        self.docNameTextField.text=[docRecord valueForKey:@"docName"];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)voiceButtonClicked:(id)sender
{
    outputFileURL =  [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:[[docRecord valueForKey:@"docRecordingName"] lastPathComponent]];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:outputFileURL error:nil];
    NSLog(@"Urlpath = %@", outputFileURL);
    [player setDelegate:self];
    [player prepareToPlay];
    
    [player play];

}
- (IBAction)shareButtonClicked:(id)sender
{
    MFMailComposeViewController *objMFMailcomposeVc = [[MFMailComposeViewController alloc] init];
    objMFMailcomposeVc.mailComposeDelegate = self;
    
    if([docRecord valueForKey:@"docImageName"]!=nil)
    {
    UIImage *emailImage;
    NSString *imagePath = [docRecord valueForKey:@"docImageName"];
    emailImage = [UIImage imageWithContentsOfFile:imagePath];;
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(emailImage)];
    
    [objMFMailcomposeVc  addAttachmentData:imageData
                                  mimeType:@"image/png"
                                  fileName:imagePath];
    }
    
    if([docRecord valueForKey:@"docRecordingName"]!=nil)
    {
    NSURL *audioURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]URLByAppendingPathComponent:[[docRecord valueForKey:@"docRecordingName"] lastPathComponent]];
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioURL];
    
    NSLog(@"DATA %@ ",audioData);
    
    [objMFMailcomposeVc addAttachmentData:audioData mimeType:@"audio/caf" fileName:[NSString stringWithFormat:@"%@",[[docRecord valueForKey:@"docRecordingName"] lastPathComponent]]];
    }
    
    NSLog(@"FILE NAME %@",[[docRecord valueForKey:@"docRecordingName"] lastPathComponent]);


    // Email Subject
    //NSString *emailTitle = @"Auftrag Detailliertheit";
    NSString *emailTitle =[NSString stringWithFormat:@"Document"];
    
    // Email Content
    
    // To address
    
     [objMFMailcomposeVc setToRecipients:@[@"Marcel.Kolb@brugg.com"]];
    [objMFMailcomposeVc setSubject:emailTitle];
    [objMFMailcomposeVc setMessageBody:self.docNameTextField.text isHTML:YES];
   // [objMFMailcomposeVc setToRecipients:toRecipents];
    //[objMFMailcomposeVc setCcRecipients:ccRecipients];
    
    
    
    // Present mail view controller on screen
    
    if ([MFMailComposeViewController canSendMail])
    {
        [self presentViewController:objMFMailcomposeVc animated:YES completion:NULL];
    }
    else
    {
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Keine Mail-Konten" message:@"Bitte setzen Sie ein Mail-Konto, um E-Mail senden." delegate:self cancelButtonTitle:@"Akzeptieren" otherButtonTitles:nil, nil];
//        
        //alert.delegate=self;
        //[alert show];
    }
    

}

- (IBAction)deleteDocClicked:(id)sender
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
 
    
    [context deleteObject:docRecord];
    
    NSError *err;
    
    if( ! [context save:&err] )
    {
        NSLog(@"Cannot save data: %@", [err localizedDescription]);
    }
    

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
             break;
            
        case MFMailComposeResultSaved:
           NSLog(@"Mail saved");
            break;
            
        case MFMailComposeResultSent:
        {
            
           NSLog(@"Mail sent");
            break;
        }
            
        case MFMailComposeResultFailed:
            
           NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            
        default:
            break;
            
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return YES;
    
}
@end
