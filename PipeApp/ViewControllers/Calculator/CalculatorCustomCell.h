//
//  CalculatorCustomCell.h
//  PipeApp
//
//  Created by Redbytes on 28/10/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorCustomCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UILabel *pipeDimentionLabel;
@property (strong, nonatomic) IBOutlet UILabel *pressureLossLabel;
@property (strong, nonatomic) IBOutlet UILabel *pressureDropLabel;
@property (strong, nonatomic) IBOutlet UILabel *velocityLabel;

@property (strong, nonatomic) IBOutlet UILabel *pipeDimentionResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *pressureLossResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *pressureDropResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *velocityResultLabel;

@property (strong, nonatomic) IBOutlet UIButton *calculateButton;
@property (strong, nonatomic) IBOutlet UIButton *clearAllButton;
@property (weak, nonatomic) IBOutlet UILabel *pipeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pipeTypeResultLabel;

//- (IBAction)calculateButtonClicked:(id)sender;
//- (IBAction)clearAllButtonClicked:(id)sender;

@end
