//
//  DocumentCustomCell.h
//  PipeApp
//
//  Created by Shaikh Rizwan on 12/29/14.
//  Copyright (c) 2014 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *DocNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *DocDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *DocTimeLabel;

@end
